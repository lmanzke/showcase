# README #

A little chess showcase program. Just for fun.

Currently Showcasing Angular + PHP, no focus on frontend. We have a move generation and via AJAX, we can play a little game of chess.

I know it is easier to retrieve possible moves client side (especially since there already are libraries, but I wanted to get familiar with Zend/Angular.

# TODO #
Save games to databases
Allow move navigation
Tweak the frontend
Allow two players to play against each other
Enhance test coverage

# RUN #
Basically we need a Zend server setup (i.e. an Apache virtual host) pointing to the /public/index.php file.

