/**
 * Created by Luci on 30.09.14.
 */
var chessModule = angular.module('chessModule', [], function ($provide) {
    $provide.factory('msgBus', ['$rootScope', function ($rootScope) {
        var msgBus = {};
        msgBus.emitMsg = function (msg, args) {
            $rootScope.$emit(msg, args);
        };
        msgBus.onMsg = function (msg, scope, func) {
            var unbind = $rootScope.$on(msg, func);
            scope.$on('$destroy', unbind);
        };
        return msgBus;
    }]);
})
    .controller('ChessController', function ($scope, $http, msgBus) {
        $scope.modalShown = false;

        var hideModal = function () {
            $scope.modalShown = false;
            $scope.show = false;
        };

        var togglePromotion = function () {
            $scope.modalShown = true;
            $scope.show = true;
        }

        var removeGreySquares = function () {
            $('#board .square-55d63').css('background', '');
        };

        var greySquare = function (square) {
            var squareEl = $('#board .square-' + square);

            var background = '#a9a9a9';
            if (squareEl.hasClass('black-3c85d') === true) {
                background = '#696969';
            }

            squareEl.css('background', background);
        };

        var onDragStart = function (source, piece) {
            // do not pick up pieces if the game is over
            // or if it's not that side's turn
            $http.get('/chess/validmoves/' + source).then(function (response) {
                for (var i in response.data) {
                    greySquare(response.data[i].to);
                }
            });
        };

        var checkPromotion = function (target, piece, callback) {
            var color = piece[0],
                pieceType = piece[1],
                rank = parseInt(target[1]);

            if (pieceType !== 'P') {
                callback(false);
                return;
            }

            var whitePromotion = color === 'w' && rank === 8,
                blackPromotion = color === 'b' && rank === 1,
                doPromotion = whitePromotion || blackPromotion;

            if (!doPromotion) {
                callback(false);
                return;
            }

            togglePromotion();
            $scope.$apply();

        }

        var checkMove = function (source, target, piece, newPos, oldPos) {
            $scope.promotionClick = function (newPiece) {
                executeMove(true, newPiece);
                hideModal();
            }

            var executeMove = function (doPromotion, promotionPiece) {
                var url = '/chess/move/' + source + '/' + target;
                if (doPromotion) {
                    url += '/' + promotionPiece;
                }

                $http.get(url).then(function (response) {
                    if (response.data.valid === false) {
                        $scope.board.position(oldPos);//manual snapback
                    } else {
                        $scope.board.position(response.data.newPos);
                        msgBus.emitMsg('movePerformed', new Move(source, target, piece, response.data.capture, response.data.check, response.data.mate, doPromotion, promotionPiece));
                    }
                });
            };

            removeGreySquares();
            checkPromotion(target, piece, executeMove);
        };

        var cfg = {
            pieceTheme: '/img/chesspieces/alpha/{piece}.png',
            position: 'start',
            draggable: true,
            onDrop: checkMove,
            onDragStart: onDragStart
        };
        $scope.board = new ChessBoard('board', cfg);

    })
    .controller('HistoryController', function ($scope, msgBus) {
        $scope.moves = [];

        msgBus.onMsg('movePerformed', $scope, function (event, arg) {
            if (arg.isWhite()) {
                $scope.moves.push({white: arg});
            } else {
                var last = $scope.moves.length - 1;
                $scope.moves[last].black = arg;
            }
        })
    });

chessModule.directive('promotionDialog', function () {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        replace: true,
        transclude: true,
        link: function (scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
                scope.dialogStyle.width = attrs.width;
            if (attrs.height)
                scope.dialogStyle.height = attrs.height;
        },
        template: "<div class='ng-modal' ng-show='show'>" +
            "<div class='ng-modal-overlay'>" +
            "</div><div class='ng-modal-dialog' ng-style='dialogStyle'>" +
            "<div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
    }
});
