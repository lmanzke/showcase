/**
 * Created by Luci on 09.10.14.
 */


function Move(source, target, piece, capture, check, mate, promotion, promotionPiece) {
    this.sets = {
        german: {
            K: 'K',
            Q: 'D',
            B: 'L',
            N: 'S',
            R: 'T',
            P: ''
        },
        english: {
            K: 'K',
            Q: 'Q',
            B: 'B',
            N: 'N',
            R: 'R',
            P: ''
        }
    };
    this.set = this.sets.german;

    this.source = source;
    this.target = target;
    this.capture = capture;
    this.piece = piece;
    this.promotion = promotion;
    this.promotionPiece = promotionPiece;
    this.check = check;
    this.mate = mate;

    if (source === undefined) {
        throw {err: 'Invalid move'}
    }

    if (target === undefined) {
        throw {err: 'Invalid move'}
    }

    if (piece === undefined) {
        throw {err: 'Invalid move'}
    }

    this.isWhite = function() {
        return piece.search(/w/) !== -1;
    };

    this.isPromotion = function() {
        if (promotion === undefined) {
            return false;
        }
        return promotion === true;
    };

    this.toSAN = function(setToUse) {
        if (setToUse === undefined) {
            setToUse = this.set;
        }
        var pieceDesc = this.piece[1],
            result = setToUse[pieceDesc] + this.source;

        if (this.capture) {
            result += 'x';
        }
        result += this.target;
        if (this.promotion) {
            result += setToUse[this.promotionPiece];
        }

        if (!this.check) {
            return result;
        }

        if (this.mate) {
            result += '#';
        } else {
            result += '+';
        }

        return result;

    }
}
