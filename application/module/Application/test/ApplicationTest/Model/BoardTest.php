<?php


/**
 * @author Lucas Manzke <lucas.manzke@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de
 * @created 28.08.14 11:53
 */

namespace Application\Model;

use Application\Traits\BoardAware;
use Application\Enum\Color;
use Application\Enum\PieceType;

/**
 * Class ChessboardTest
 * This class tests if the board and squares work correctly.
 * @package Application\test\Model
 */
class BoardTest extends \PHPUnit_Framework_TestCase
{
    use BoardAware;

    public function leftCoordinateValues()
    {
        return [
            [[8, 8], [8, 7]],
            [[8, 7], [8, 6]],
            [[7, 7], [7, 6]],
            [[2, 2], [2, 1]],
        ];
    }

    /**
     * @dataProvider leftCoordinateValues
     * @param $originCoordinates
     * @param $expectedTargetCoordinates
     */
    public function testLeftCoordinates($originCoordinates, $expectedTargetCoordinates)
    {
        $originRank = $originCoordinates[0];
        $originFile = $originCoordinates[1];

        $targetRank = $expectedTargetCoordinates[0];
        $targetFile = $expectedTargetCoordinates[1];

        $square = $this->board->getSquareByRankAndFile($originRank, $originFile);
        $targetSquare = $square->getLeft();

        $this->assertEquals($targetRank, $targetSquare->getRank());
        $this->assertEquals($targetFile, $targetSquare->getFile());
    }

    public function testLeftFileHasNoLeft()
    {
        $file = 1;
        for ($rank = 1; $rank < 8; $rank++) {
            $square = $this->board->getSquareByRankAndFile($rank, $file);
            $this->assertFalse($square->hasLeft());
        }
    }

    public function rightCoordinateValues()
    {
        return [
            [[1, 1], [1, 2]],
            [[5, 6], [5, 7]],
            [[7, 7], [7, 8]],
            [[2, 2], [2, 3]],
        ];
    }

    /**
     * @dataProvider rightCoordinateValues
     * @param $originCoordinates
     * @param $expectedTargetCoordinates
     */
    public function testRightCoordinates($originCoordinates, $expectedTargetCoordinates)
    {
        $originRank = $originCoordinates[0];
        $originFile = $originCoordinates[1];

        $targetRank = $expectedTargetCoordinates[0];
        $targetFile = $expectedTargetCoordinates[1];

        $square = $this->board->getSquareByRankAndFile($originRank, $originFile);
        $targetSquare = $square->getRight();

        $this->assertEquals($targetRank, $targetSquare->getRank());
        $this->assertEquals($targetFile, $targetSquare->getFile());
    }

    public function testRightFileHasNoRight()
    {
        $file = 8;
        for ($rank = 1; $rank < 8; $rank++) {
            $square = $this->board->getSquareByRankAndFile($rank, $file);
            $this->assertFalse($square->hasRight());
        }
    }

    public function downCoordinateValues()
    {
        return [
            [[2, 1], [1, 1]],
            [[5, 6], [4, 6]],
            [[7, 7], [6, 7]],
            [[2, 2], [1, 2]],
        ];
    }

    /**
     * @dataProvider downCoordinateValues
     * @param $originCoordinates
     * @param $expectedTargetCoordinates
     */
    public function testDownCoordinates($originCoordinates, $expectedTargetCoordinates)
    {
        $originRank = $originCoordinates[0];
        $originFile = $originCoordinates[1];

        $targetRank = $expectedTargetCoordinates[0];
        $targetFile = $expectedTargetCoordinates[1];

        $square = $this->board->getSquareByRankAndFile($originRank, $originFile);
        $targetSquare = $square->getDown();

        $this->assertEquals($targetRank, $targetSquare->getRank());
        $this->assertEquals($targetFile, $targetSquare->getFile());
    }

    public function testDownRankHasNoDown()
    {
        $rank = 1;
        for ($file = 1; $file < 8; $file++) {
            $square = $this->board->getSquareByRankAndFile($rank, $file);
            $this->assertFalse($square->hasDown());
        }
    }

    public function upCoordinateValues()
    {
        return [
            [[2, 1], [3, 1]],
            [[5, 6], [7, 6]],
            [[7, 7], [8, 7]],
            [[2, 2], [3, 2]],
        ];
    }

    /**
     * @dataProvider upCoordinateValues
     * @param $originCoordinates
     * @param $expectedTargetCoordinates
     */
    public function upDownCoordinates($originCoordinates, $expectedTargetCoordinates)
    {
        $originRank = $originCoordinates[0];
        $originFile = $originCoordinates[1];

        $targetRank = $expectedTargetCoordinates[0];
        $targetFile = $expectedTargetCoordinates[1];

        $square = $this->board->getSquareByRankAndFile($originRank, $originFile);
        $targetSquare = $square->getUp();

        $this->assertEquals($targetRank, $targetSquare->getRank());
        $this->assertEquals($targetFile, $targetSquare->getFile());
    }

    public function testUpRankHasNoUp()
    {
        $rank = 8;
        for ($file = 1; $file < 8; $file++) {
            $square = $this->board->getSquareByRankAndFile($rank, $file);
            $this->assertFalse($square->hasUp());
        }
    }

    public function diagonalValues()
    {
        return [
            [1, 1, false, true, false, false], //A1
            [2, 2, true, true, true, true], //B2
            [3, 1, false, true, false, true], //A3
            [1, 3, true, true, false, false], //C1
            [1, 8, true, false, false, false], //H1
            [2, 7, true, true, true, true], //G2
            [3, 8, true, false, true, false], //H3
            [8, 8, false, false, true, false], //H8
            [7, 7, true, true, true, true], //G7
            [8, 6, false, false, true, true], //F8
        ];
    }

    /**
     * @dataProvider diagonalValues
     * @param $rank
     * @param $file
     * @param $upLeft
     * @param $upRight
     * @param $downLeft
     * @param $downRight
     */
    public function testHasDiagonal($rank, $file, $upLeft, $upRight, $downLeft, $downRight)
    {
        $square = $this->board->getSquareByRankAndFile($rank, $file);

        $this->assertEquals($upLeft, $square->hasUpLeft());
        $this->assertEquals($upRight, $square->hasUpRight());
        $this->assertEquals($downLeft, $square->hasDownLeft());
        $this->assertEquals($downRight, $square->hasDownRight());
    }

    public function pieceProvider()
    {
        return [
            [1, 1, Color::$WHITE, PieceType::$ROOK],
            [1, 2, Color::$WHITE, PieceType::$KNIGHT],
            [1, 3, Color::$WHITE, PieceType::$BISHOP],
            [1, 4, Color::$WHITE, PieceType::$QUEEN],
            [1, 5, Color::$WHITE, PieceType::$KING],
            [1, 6, Color::$WHITE, PieceType::$BISHOP],
            [1, 7, Color::$WHITE, PieceType::$KNIGHT],
            [1, 8, Color::$WHITE, PieceType::$ROOK],
            [2, 1, Color::$WHITE, PieceType::$PAWN],
            [2, 2, Color::$WHITE, PieceType::$PAWN],
            [2, 3, Color::$WHITE, PieceType::$PAWN],
            [2, 4, Color::$WHITE, PieceType::$PAWN],
            [2, 5, Color::$WHITE, PieceType::$PAWN],
            [2, 6, Color::$WHITE, PieceType::$PAWN],
            [2, 7, Color::$WHITE, PieceType::$PAWN],
            [2, 8, Color::$WHITE, PieceType::$PAWN],

            [8, 1, Color::$BLACK, PieceType::$ROOK],
            [8, 2, Color::$BLACK, PieceType::$KNIGHT],
            [8, 3, Color::$BLACK, PieceType::$BISHOP],
            [8, 4, Color::$BLACK, PieceType::$QUEEN],
            [8, 5, Color::$BLACK, PieceType::$KING],
            [8, 6, Color::$BLACK, PieceType::$BISHOP],
            [8, 7, Color::$BLACK, PieceType::$KNIGHT],
            [8, 8, Color::$BLACK, PieceType::$ROOK],
            [7, 1, Color::$BLACK, PieceType::$PAWN],
            [7, 2, Color::$BLACK, PieceType::$PAWN],
            [7, 3, Color::$BLACK, PieceType::$PAWN],
            [7, 4, Color::$BLACK, PieceType::$PAWN],
            [7, 5, Color::$BLACK, PieceType::$PAWN],
            [7, 6, Color::$BLACK, PieceType::$PAWN],
            [7, 7, Color::$BLACK, PieceType::$PAWN],
            [7, 8, Color::$BLACK, PieceType::$PAWN],
        ];
    }

    /**
     * @dataProvider pieceProvider
     * @param $rank
     * @param $file
     * @param $color
     * @param $type
     */
    public function testPiecePlacement($rank, $file, $color, $type)
    {
        $this->board->initStandardChess();

        $square = $this->board->getSquareByRankAndFile($rank, $file);
        $piece = $square->getPiece();

        $this->assertEquals($color, $piece->getColor());
        $this->assertEquals($type, $piece->getType());
    }

    public function testStartSquaresEmpty()
    {
        $this->board->initStandardChess();
        for ($rank = 3; $rank < 7; $rank++) {
            for ($file = 1; $file < 9; $file++) {
                $square = $this->board->getSquareByRankAndFile($rank, $file);
                $this->assertNull($square->getPiece());
            }
        }
    }

    public function FENPositionProvider()
    {
        return [
            [1, 1, Color::$WHITE, PieceType::$ROOK],
            [1, 4, Color::$WHITE, PieceType::$QUEEN],
            [1, 6, Color::$WHITE, PieceType::$ROOK],
            [1, 7, Color::$WHITE, PieceType::$KING],
            [2, 1, Color::$WHITE, PieceType::$PAWN],
            [2, 2, Color::$WHITE, PieceType::$PAWN],
            [2, 5, Color::$WHITE, PieceType::$KNIGHT],
            [2, 7, Color::$WHITE, PieceType::$BISHOP],
            [3, 3, Color::$WHITE, PieceType::$PAWN],
            [3, 5, Color::$WHITE, PieceType::$BISHOP],
            [3, 6, Color::$WHITE, PieceType::$KNIGHT],
            [3, 7, Color::$WHITE, PieceType::$PAWN],
            [3, 8, Color::$WHITE, PieceType::$PAWN],
            [4, 4, Color::$WHITE, PieceType::$PAWN],
            [4, 6, Color::$WHITE, PieceType::$PAWN],

            [5, 3, Color::$BLACK, PieceType::$PAWN],
            [5, 4, Color::$BLACK, PieceType::$PAWN],
            [5, 5, Color::$BLACK, PieceType::$PAWN],
            [6, 2, Color::$BLACK, PieceType::$PAWN],
            [6, 3, Color::$BLACK, PieceType::$KNIGHT],
            [6, 6, Color::$BLACK, PieceType::$BISHOP],
            [6, 7, Color::$BLACK, PieceType::$PAWN],
            [7, 1, Color::$BLACK, PieceType::$PAWN],
            [7, 2, Color::$BLACK, PieceType::$BISHOP],
            [7, 4, Color::$BLACK, PieceType::$KNIGHT],
            [7, 8, Color::$BLACK, PieceType::$PAWN],
            [8, 1, Color::$BLACK, PieceType::$ROOK],
            [8, 4, Color::$BLACK, PieceType::$QUEEN],
            [8, 6, Color::$BLACK, PieceType::$ROOK],
            [8, 7, Color::$BLACK, PieceType::$KING],
        ];
    }

    /**
     * @dataProvider FENPositionProvider
     * @param $rank
     * @param $file
     * @param $color
     * @param $type
     */
    public function testFENReaderPieces($rank, $file, $color, $type)
    {
        $fen = 'r2q1rk1/pb1n3p/1pn2bp1/2ppp3/3P1P2/2P1BNPP/PP2N1B1/R2Q1RK1 b - -';
        $this->board->initFromFen($fen);

        $square = $this->board->getSquareByRankAndFile($rank, $file);
        $piece = $square->getPiece();

        $this->assertEquals($color, $piece->getColor());
        $this->assertEquals($type, $piece->getType());
    }

    public function testWhiteColorMoveGeneration()
    {
        $this->board->initStandardChess();

        $moveList = $this->board->generateMovesForColor(Color::$WHITE);

        $expected = [
            new Move($this->board->getSquareByRankAndFile(1, 2), $this->board->getSquareByRankAndFile(3, 1)),
            new Move($this->board->getSquareByRankAndFile(1, 2), $this->board->getSquareByRankAndFile(3, 3)),
            new Move($this->board->getSquareByRankAndFile(1, 7), $this->board->getSquareByRankAndFile(3, 6)),
            new Move($this->board->getSquareByRankAndFile(1, 7), $this->board->getSquareByRankAndFile(3, 8)),
            new Move($this->board->getSquareByRankAndFile(2, 1), $this->board->getSquareByRankAndFile(3, 1)),
            new Move($this->board->getSquareByRankAndFile(2, 1), $this->board->getSquareByRankAndFile(4, 1), $this->board->getSquareByRankAndFile(3, 1)),
            new Move($this->board->getSquareByRankAndFile(2, 2), $this->board->getSquareByRankAndFile(3, 2)),
            new Move($this->board->getSquareByRankAndFile(2, 2), $this->board->getSquareByRankAndFile(4, 2), $this->board->getSquareByRankAndFile(3, 2)),
            new Move($this->board->getSquareByRankAndFile(2, 3), $this->board->getSquareByRankAndFile(3, 3)),
            new Move($this->board->getSquareByRankAndFile(2, 3), $this->board->getSquareByRankAndFile(4, 3), $this->board->getSquareByRankAndFile(3, 3)),
            new Move($this->board->getSquareByRankAndFile(2, 4), $this->board->getSquareByRankAndFile(3, 4)),
            new Move($this->board->getSquareByRankAndFile(2, 4), $this->board->getSquareByRankAndFile(4, 4), $this->board->getSquareByRankAndFile(3, 4)),
            new Move($this->board->getSquareByRankAndFile(2, 5), $this->board->getSquareByRankAndFile(3, 5)),
            new Move($this->board->getSquareByRankAndFile(2, 5), $this->board->getSquareByRankAndFile(4, 5), $this->board->getSquareByRankAndFile(3, 5)),
            new Move($this->board->getSquareByRankAndFile(2, 6), $this->board->getSquareByRankAndFile(3, 6)),
            new Move($this->board->getSquareByRankAndFile(2, 6), $this->board->getSquareByRankAndFile(4, 6), $this->board->getSquareByRankAndFile(3, 6)),
            new Move($this->board->getSquareByRankAndFile(2, 7), $this->board->getSquareByRankAndFile(3, 7)),
            new Move($this->board->getSquareByRankAndFile(2, 7), $this->board->getSquareByRankAndFile(4, 7), $this->board->getSquareByRankAndFile(3, 7)),
            new Move($this->board->getSquareByRankAndFile(2, 8), $this->board->getSquareByRankAndFile(3, 8)),
            new Move($this->board->getSquareByRankAndFile(2, 8), $this->board->getSquareByRankAndFile(4, 8), $this->board->getSquareByRankAndFile(3, 8)),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testBlackColorMoveGeneration()
    {
        $fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1';
        $this->board->initFromFen($fen);

        $moveList = $this->board->generateMovesForColor(Color::$BLACK);

        $expected = [
            new Move($this->board->getSquareByRankAndFile(7, 1), $this->board->getSquareByRankAndFile(6, 1)),
            new Move($this->board->getSquareByRankAndFile(7, 1), $this->board->getSquareByRankAndFile(5, 1), $this->board->getSquareByRankAndFile(6, 1)),
            new Move($this->board->getSquareByRankAndFile(7, 2), $this->board->getSquareByRankAndFile(6, 2)),
            new Move($this->board->getSquareByRankAndFile(7, 2), $this->board->getSquareByRankAndFile(5, 2), $this->board->getSquareByRankAndFile(6, 2)),
            new Move($this->board->getSquareByRankAndFile(7, 3), $this->board->getSquareByRankAndFile(6, 3)),
            new Move($this->board->getSquareByRankAndFile(7, 3), $this->board->getSquareByRankAndFile(5, 3), $this->board->getSquareByRankAndFile(6, 3)),
            new Move($this->board->getSquareByRankAndFile(7, 4), $this->board->getSquareByRankAndFile(6, 4)),
            new Move($this->board->getSquareByRankAndFile(7, 4), $this->board->getSquareByRankAndFile(5, 4), $this->board->getSquareByRankAndFile(6, 4)),
            new Move($this->board->getSquareByRankAndFile(7, 5), $this->board->getSquareByRankAndFile(6, 5)),
            new Move($this->board->getSquareByRankAndFile(7, 5), $this->board->getSquareByRankAndFile(5, 5), $this->board->getSquareByRankAndFile(6, 5)),
            new Move($this->board->getSquareByRankAndFile(7, 6), $this->board->getSquareByRankAndFile(6, 6)),
            new Move($this->board->getSquareByRankAndFile(7, 6), $this->board->getSquareByRankAndFile(5, 6), $this->board->getSquareByRankAndFile(6, 6)),
            new Move($this->board->getSquareByRankAndFile(7, 7), $this->board->getSquareByRankAndFile(6, 7)),
            new Move($this->board->getSquareByRankAndFile(7, 7), $this->board->getSquareByRankAndFile(5, 7), $this->board->getSquareByRankAndFile(6, 7)),
            new Move($this->board->getSquareByRankAndFile(7, 8), $this->board->getSquareByRankAndFile(6, 8)),
            new Move($this->board->getSquareByRankAndFile(7, 8), $this->board->getSquareByRankAndFile(5, 8), $this->board->getSquareByRankAndFile(6, 8)),
            new Move($this->board->getSquareByRankAndFile(8, 2), $this->board->getSquareByRankAndFile(6, 1)),
            new Move($this->board->getSquareByRankAndFile(8, 2), $this->board->getSquareByRankAndFile(6, 3)),
            new Move($this->board->getSquareByRankAndFile(8, 7), $this->board->getSquareByRankAndFile(6, 6)),
            new Move($this->board->getSquareByRankAndFile(8, 7), $this->board->getSquareByRankAndFile(6, 8)),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * @dataProvider whiteThreatMapProvider
     * @param $rank
     * @param $file
     * @param $marked
     */
    public function testThreatGeneration($rank, $file, $marked) {
        $fen = '8/R3P3/2Pk2p1/8/4B3/8/1B1r2r1/5K2 w';
        $this->board->initFromFen($fen);

        $threatMap = $this->board->getThreats(Color::$WHITE);
        $this->assertEquals($marked, $threatMap->isMarked($rank, $file));
    }

    public function whiteThreatMapProvider() {
        return [
            [1, 1, true],
            [1, 2, true],
            [1, 3, true],
            [1, 4, false],
            [1, 5, true],
            [1, 6, false],
            [1, 7, true],
            [1, 8, false],

            [2, 1, true],
            [2, 2, false],
            [2, 3, true],
            [2, 4, false],
            [2, 5, true],
            [2, 6, true],
            [2, 7, true],
            [2, 8, false],

            [3, 1, true],
            [3, 2, false],
            [3, 3, true],
            [3, 4, true],
            [3, 5, false],
            [3, 6, true],
            [3, 7, false],
            [3, 8, false],

            [4, 1, true],
            [4, 2, false],
            [4, 3, false],
            [4, 4, true],
            [4, 5, false],
            [4, 6, false],
            [4, 7, false],
            [4, 8, false],

            [5, 1, true],
            [5, 2, false],
            [5, 3, false],
            [5, 4, true],
            [5, 5, true],
            [5, 6, true],
            [5, 7, false],
            [5, 8, false],

            [6, 1, true],
            [6, 2, false],
            [6, 3, true],
            [6, 4, false],
            [6, 5, false],
            [6, 6, true],
            [6, 7, true],
            [6, 8, false],

            [7, 1, false],
            [7, 2, true],
            [7, 3, true],
            [7, 4, true],
            [7, 5, true],
            [7, 6, false],
            [7, 7, true],
            [7, 8, false],

            [8, 1, true],
            [8, 2, false],
            [8, 3, false],
            [8, 4, true],
            [8, 5, false],
            [8, 6, true],
            [8, 7, false],
            [8, 8, true],
        ];
    }

    /**
     * @dataProvider blackThreatMapProvider
     * @param $rank
     * @param $file
     * @param $marked
     */
    public function testThreatGeneration2($rank, $file, $marked) {
        $fen = '8/R3P3/2Pk2p1/8/4B3/8/1B1r2r1/5K2 w';
        $this->board->initFromFen($fen);

        $threatMap = $this->board->getThreats(Color::$BLACK);
        $this->assertEquals($marked, $threatMap->isMarked($rank, $file));
    }

    public function blackThreatMapProvider() {
        return [
            [1, 1, false],
            [1, 2, false],
            [1, 3, false],
            [1, 4, true],
            [1, 5, false],
            [1, 6, false],
            [1, 7, true],
            [1, 8, false],

            [2, 1, false],
            [2, 2, true],
            [2, 3, true],
            [2, 4, true],
            [2, 5, true],
            [2, 6, true],
            [2, 7, true],
            [2, 8, true],

            [3, 1, false],
            [3, 2, false],
            [3, 3, false],
            [3, 4, true],
            [3, 5, false],
            [3, 6, false],
            [3, 7, true],
            [3, 8, false],

            [4, 1, false],
            [4, 2, false],
            [4, 3, false],
            [4, 4, true],
            [4, 5, false],
            [4, 6, false],
            [4, 7, true],
            [4, 8, false],

            [5, 1, false],
            [5, 2, false],
            [5, 3, true],
            [5, 4, true],
            [5, 5, true],
            [5, 6, true],
            [5, 7, true],
            [5, 8, true],

            [6, 1, false],
            [6, 2, false],
            [6, 3, true],
            [6, 4, true],
            [6, 5, true],
            [6, 6, false],
            [6, 7, true],
            [6, 8, false],

            [7, 1, false],
            [7, 2, false],
            [7, 3, true],
            [7, 4, true],
            [7, 5, true],
            [7, 6, false],
            [7, 7, false],
            [7, 8, false],

            [8, 1, false],
            [8, 2, false],
            [8, 3, false],
            [8, 4, false],
            [8, 5, false],
            [8, 6, false],
            [8, 7, false],
            [8, 8, false],
        ];
    }

    public function testMoveLegality1() {
        $fen = 'r3k2r/8/2p1p3/1Q3B2/1b6/2P1R3/8/1q1NK3 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(6, 3);
        $target1 = $this->board->getSquareByRankAndFile(5, 2);

        $moveList = $this->board->generateMovesForSquare($origin);

        $expected = [
            new Move($origin, $target1)
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testFenOutput1()
    {
        $this->board->initStandardChess();
        $fen = $this->board->getFen();

        $expected = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
        $this->assertEquals($expected, $fen);
    }

    public function testFenOutput2()
    {
        $this->board->initStandardChess();

        $moveList = $this->board->generateMoves(2, 1);

        $this->board->makeMove($moveList[1]); //double step
        $fen = $this->board->getFen();

        $expected = 'rnbqkbnr/pppppppp/8/8/P7/8/1PPPPPPP/RNBQKBNR b KQkq a3 0 1';
        $this->assertEquals($expected, $fen);
    }

    /**
     * expected:
     * r n b q k b n r      both castlings
     * p - p p p p p p
     * - E - - - - - -
     * P p p - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - P P P P P P P
     * R N B Q K B N R      both castlings
     */
    public function testFenOutput3() {
        $this->board->initStandardChess();

        $moveList = $this->board->generateMoves(2, 1);
        $this->board->makeMove($moveList[1]); //double step

        $moveList = $this->board->generateMoves(7, 3);
        $this->board->makeMove($moveList[1]); //double step

        $moveList = $this->board->generateMoves(4, 1);
        $this->board->makeMove($moveList[0]);

        $moveList = $this->board->generateMoves(7, 2);
        $this->board->makeMove($moveList[1]); //double step

        $fen = $this->board->getFen();

        $expected = 'rnbqkbnr/p2ppppp/8/Ppp5/8/8/1PPPPPPP/RNBQKBNR w KQkq b6 0 3';
        $this->assertEquals($expected, $fen);
    }

    public function testFenOutput4() {
        $this->board->initStandardChess();

        $moveList = $this->board->generateMoves(1, 2);
        $this->board->makeMove($moveList[0]);

        $fen = $this->board->getFen();

        $expected = 'rnbqkbnr/pppppppp/8/8/8/N7/PPPPPPPP/R1BQKBNR b KQkq - 1 1';
        $this->assertEquals($expected, $fen);
    }

    /**
     * expected:
     * r n b q k b n r
     * p - p p p p p p
     * - P - - - - - -
     * - - p - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - P P P P P P P
     * R N B Q K B N R
     */
    public function testFenInAndOut()
    {
        $fen = 'rnbqkbnr/p2ppppp/8/Ppp5/8/8/1PPPPPPP/RNBQKBNR w KQkq b6 0 3';
        $this->board->initFromFen($fen);

        $moveList = $this->board->generateMoves(5, 1);
        $this->board->makeMove($moveList[1]); //en passant capture

        $expected = 'rnbqkbnr/p2ppppp/1P6/2p5/8/8/1PPPPPPP/RNBQKBNR b KQkq - 0 3';

        $fen = $this->board->getFen();
        $this->assertEquals($expected, $fen);
    }

    //followup: test input and output


}
