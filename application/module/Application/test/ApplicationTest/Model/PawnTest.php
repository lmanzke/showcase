<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04.09.14
 * Time: 18:42
 */

namespace Application\Model;


use Application\Enum\PieceType;
use Application\Traits\BoardAware;

class PawnTest extends \PHPUnit_Framework_TestCase {
    use BoardAware;

    public function testPawnMoveList()
    {
        $this->board->initStandardChess();

        $moveList = $this->board->generateMoves(2, 1); //the move generation partly depends on other squares. Only the board knows the whole picture
        $originSquare = $this->board->getSquareByRankAndFile(2, 1);
        $targetOne = $this->board->getSquareByRankAndFile(3, 1);
        $targetTwo = $this->board->getSquareByRankAndFile(4, 1);
        $enPassant = $targetOne;

        $expected = [new Move($originSquare, $targetOne),
            new Move($originSquare, $targetTwo, $enPassant)];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * r - - q - r k -
     * p b - n - - - p
     * - p n - - b p -
     * - - p p p - - -
     * - - - P - P - -
     * - - P - B N P P
     * P P - - N - B -
     * R - - Q - R K -
     */
    public function testPawnMoveList2()
    {
        $fen = 'r2q1rk1/pb1n3p/1pn2bp1/2ppp3/3P1P2/2P1BNPP/PP2N1B1/R2Q1RK1 b - -';
        $this->board->initFromFen($fen);
        $origin = $this->board->getSquareByRankAndFile(5, 5);
        $moveList = $this->board->generateMoves(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(4, 5);
        $target2 = $this->board->getSquareByRankAndFile(4, 4);
        $target3 = $this->board->getSquareByRankAndFile(4, 6);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3)
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testPawnMoveList3() {
        $fen ='r2q1rk1/pb1n3p/1pn2bp1/3pp3/2pP1P2/2P1BNPP/PP2N1B1/R2Q1RK1 w - -';
        $this->board->initFromFen($fen);
        $origin = $this->board->getSquareByRankAndFile(4, 6);
        $moveList = $this->board->generateMoves(4, 6);
        $target1 = $this->board->getSquareByRankAndFile(5, 6);
        $target2 = $this->board->getSquareByRankAndFile(5, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2)
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testPawnMoveList4() {
        $fen ='r2q1rk1/pb1n3p/1pn2bp1/3pp3/2pP1P2/2P1BNPP/PP2N1B1/R2Q1RK1 w - -';
        $this->board->initFromFen($fen);
        $moveList = $this->board->generateMoves(3, 3);

        $expected = [];

        $this->assertEquals($expected, $moveList);
    }

    public function testEnPassant()
    {
        $fen = 'r2q1rk1/pb1n3p/2n2bp1/2ppp3/1pPP1P2/4BNPP/PP2N1B1/R2Q1RK1 b - c3';
        $this->board->initFromFen($fen);

        $square = $this->board->getSquareByRankAndFile(4, 2);
        $target1 = $this->board->getSquareByRankAndFile(3, 2);
        $target2 = $this->board->getSquareByRankAndFile(3, 3);
        $clearance = $square->moveRight(1);

        $moveList = $this->board->generateMoves(4, 2);

        $expected = [new Move($square, $target1), new Move($square, $target2, null, $clearance)];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - - - k - -
     * - - - - - - - -
     * - E - - - - - -
     * r p P K - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     */
    public function testIllegalEnPassant() {
        $fen = '5k2/8/8/rpPK4/8/8/8/8 w - b6';
        $this->board->initFromFen($fen);

        $square = $this->board->getSquareByRankAndFile(5, 3);
        $target1 = $this->board->getSquareByRankAndFile(6, 3);

        $moveList = $this->board->generateMovesForSquare($square);

        $expected = [
            new Move($square, $target1)
        ];

        $this->assertEquals($expected, $moveList);
    }


    public function testWhitePromotion() {
        $fen = '2n5/3P4/k7/8/8/8/8/4K3 w';
        $this->board->initFromFen($fen);

        $moveList = $this->board->generateMoves(7, 4);
        $origin = $this->board->getSquareByRankAndFile(7, 4);

        $target1 = $this->board->getSquareByRankAndFile(8, 4);
        $target2 = $this->board->getSquareByRankAndFile(8, 3);

        $expected = [
            new Move($origin, $target1, null, null, PieceType::$KNIGHT),
            new Move($origin, $target1, null, null, PieceType::$BISHOP),
            new Move($origin, $target1, null, null, PieceType::$ROOK),
            new Move($origin, $target1, null, null, PieceType::$QUEEN),
            new Move($origin, $target2, null, null, PieceType::$KNIGHT),
            new Move($origin, $target2, null, null, PieceType::$BISHOP),
            new Move($origin, $target2, null, null, PieceType::$ROOK),
            new Move($origin, $target2, null, null, PieceType::$QUEEN)
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testBlackPromotion() {
        $fen = '8/8/k7/8/8/8/3p4/4BK2 b';
        $this->board->initFromFen($fen);

        $moveList = $this->board->generateMoves(2, 4);
        $origin = $this->board->getSquareByRankAndFile(2, 4);

        $target1 = $this->board->getSquareByRankAndFile(1, 4);
        $target2 = $this->board->getSquareByRankAndFile(1, 5);

        $expected = [
            new Move($origin, $target1, null, null, PieceType::$KNIGHT),
            new Move($origin, $target1, null, null, PieceType::$BISHOP),
            new Move($origin, $target1, null, null, PieceType::$ROOK),
            new Move($origin, $target1, null, null, PieceType::$QUEEN),
            new Move($origin, $target2, null, null, PieceType::$KNIGHT),
            new Move($origin, $target2, null, null, PieceType::$BISHOP),
            new Move($origin, $target2, null, null, PieceType::$ROOK),
            new Move($origin, $target2, null, null, PieceType::$QUEEN)
        ];

        $this->assertEquals($expected, $moveList);
    }
}