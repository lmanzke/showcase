<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 01.09.14
 * Time: 19:58
 */

namespace Application\Model;
use Application\Enum\Direction;


/**
 * Class SquareTest
 * This class test if the logical functions work correctly.
 * @package Application\Model
 */
class SquareTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Square
     */
    protected $square;

    public function setUp()
    {
        $this->square = new Square(4, 4);
    }


    public function testHasLeft()
    {
        $this->assertFalse($this->square->hasLeft());

        $left = new Square(4, 3);
        $this->square->setLeft($left);

        $this->assertTrue($this->square->hasLeft());
    }

    public function testHasRight()
    {
        $this->assertFalse($this->square->hasRight());

        $right = new Square(4, 5);
        $this->square->setRight($right);

        $this->assertTrue($this->square->hasRight());
    }

    public function testHasUp()
    {
        $this->assertFalse($this->square->hasUp());

        $up = new Square(5, 4);
        $this->square->setUp($up);

        $this->assertTrue($this->square->hasUp());
    }

    public function testHasDown()
    {
        $this->assertFalse($this->square->hasDown());

        $down = new Square(3, 4);
        $this->square->setDown($down);

        $this->assertTrue($this->square->hasDown());
    }

    public function testDistanceCalculation() {
        $otherSquare = new Square(6, 5);
        $expectedDistance = 3;
        $distance = $this->square->getDistance($otherSquare);

        $this->assertEquals($expectedDistance, $distance);
    }

    public function testDistanceCalculation2() {
        $otherSquare = new Square(2, 7);
        $expectedDistance = 5;
        $distance = $this->square->getDistance($otherSquare);

        $this->assertEquals($expectedDistance, $distance);
    }

    public function testGetDirection1()
    {
        $otherSquare = new Square(4, 2);
        $expectedDirection = Direction::$LEFT;

        $this->assertEquals($expectedDirection, $this->square->getDirection($otherSquare));
    }

    public function testGetDirection2()
    {
        $otherSquare = new Square(4, 6);
        $expectedDirection = Direction::$RIGHT;

        $this->assertEquals($expectedDirection, $this->square->getDirection($otherSquare));
    }


}