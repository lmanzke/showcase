<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 05.09.14
 * Time: 00:13
 */

namespace Application\Model;


use Application\Traits\BoardAware;

class BishopTest extends \PHPUnit_Framework_TestCase {
    use BoardAware;


    /**
     * - k - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - b - - -
     * - - - - - - - -
     * - - - - - - - -
     * K - - - - - - -
     */
    public function testFreeBishop()
    {
        $fen = '1k6/8/8/8/4b3/8/8/K7 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 5);

        $target1 = $this->board->getSquareByRankAndFile(5, 4);
        $target2 = $this->board->getSquareByRankAndFile(6, 3);
        $target3 = $this->board->getSquareByRankAndFile(7, 2);
        $target4 = $this->board->getSquareByRankAndFile(8, 1);
        $target5 = $this->board->getSquareByRankAndFile(5, 6);
        $target6 = $this->board->getSquareByRankAndFile(6, 7);
        $target7 = $this->board->getSquareByRankAndFile(7, 8);
        $target8 = $this->board->getSquareByRankAndFile(3, 6);
        $target9 = $this->board->getSquareByRankAndFile(2, 7);
        $target10 = $this->board->getSquareByRankAndFile(1, 8);
        $target11 = $this->board->getSquareByRankAndFile(3, 4);
        $target12 = $this->board->getSquareByRankAndFile(2, 3);
        $target13 = $this->board->getSquareByRankAndFile(1, 2);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
            new Move($origin, $target11),
            new Move($origin, $target12),
            new Move($origin, $target13),
        ];

        $moveList = $this->board->generateMoves(4, 5);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - k - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - r - - - -
     * - - - - b - - -
     * - - - - - - - -
     * - - - - - - - -
     * K - - - - - - -
     */
    public function testOneBlockedDirectionBishop()
    {
        $fen = '1k6/8/8/3r4/4b3/8/8/K7 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 5);

        $target1 = $this->board->getSquareByRankAndFile(5, 6);
        $target2 = $this->board->getSquareByRankAndFile(6, 7);
        $target3 = $this->board->getSquareByRankAndFile(7, 8);
        $target4 = $this->board->getSquareByRankAndFile(3, 6);
        $target5 = $this->board->getSquareByRankAndFile(2, 7);
        $target6 = $this->board->getSquareByRankAndFile(1, 8);
        $target7 = $this->board->getSquareByRankAndFile(3, 4);
        $target8 = $this->board->getSquareByRankAndFile(2, 3);
        $target9 = $this->board->getSquareByRankAndFile(1, 2);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
        ];

        $moveList = $this->board->generateMoves(4, 5);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - k - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - R - - - -
     * - - - - b - - -
     * - - - - - - - -
     * - - - - - - - -
     * K - - - - - - -
     */
    public function testOneBlockedDirectionCaptureBishop()
    {
        $fen = '1k6/8/8/3R4/4b3/8/8/K7 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 5);

        $target1 = $this->board->getSquareByRankAndFile(5, 4);
        $target2 = $this->board->getSquareByRankAndFile(5, 6);
        $target3 = $this->board->getSquareByRankAndFile(6, 7);
        $target4 = $this->board->getSquareByRankAndFile(7, 8);
        $target5 = $this->board->getSquareByRankAndFile(3, 6);
        $target6 = $this->board->getSquareByRankAndFile(2, 7);
        $target7 = $this->board->getSquareByRankAndFile(1, 8);
        $target8 = $this->board->getSquareByRankAndFile(3, 4);
        $target9 = $this->board->getSquareByRankAndFile(2, 3);
        $target10 = $this->board->getSquareByRankAndFile(1, 2);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
        ];

        $moveList = $this->board->generateMoves(4, 5);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - k - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - R - - - -
     * - - - - b - - -
     * - - - - - - - -
     * - - - - - - - -
     * B - - - - - K -
     */
    public function testOneDiagonalBishop()
    {
        $fen = '1k6/8/8/3R4/4b3/8/8/B5K1 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(1, 1);

        $target1 = $this->board->getSquareByRankAndFile(2, 2);
        $target2 = $this->board->getSquareByRankAndFile(3, 3);
        $target3 = $this->board->getSquareByRankAndFile(4, 4);
        $target4 = $this->board->getSquareByRankAndFile(5, 5);
        $target5 = $this->board->getSquareByRankAndFile(6, 6);
        $target6 = $this->board->getSquareByRankAndFile(7, 7);
        $target7 = $this->board->getSquareByRankAndFile(8, 8);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
        ];

        $moveList = $this->board->generateMoves(1, 1);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - k - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - R - - - -
     * - - - - b - - -
     * - - - - - - - -
     * - N - - - - - -
     * B - - - - - K -
     */
    public function testNoMoveBishop()
    {
        $fen = '1k6/8/8/3R4/4b3/8/1N6/B6K w';
        $this->board->initFromFen($fen);

        $expected = [];

        $moveList = $this->board->generateMoves(1, 1);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - k - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - R - - - -
     * - - - - b - - -
     * N - - - - - - -
     * - B - - - - - -
     * R - - - - - K -
     */
    public function test2DiagonalBishop()
    {
        $fen = '1k6/8/8/3R4/4b3/N7/1B6/R5K1 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(2, 2);

        $target1 = $this->board->getSquareByRankAndFile(3, 3);
        $target2 = $this->board->getSquareByRankAndFile(4, 4);
        $target3 = $this->board->getSquareByRankAndFile(5, 5);
        $target4 = $this->board->getSquareByRankAndFile(6, 6);
        $target5 = $this->board->getSquareByRankAndFile(7, 7);
        $target6 = $this->board->getSquareByRankAndFile(8, 8);
        $target7 = $this->board->getSquareByRankAndFile(1, 3);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
        ];

        $moveList = $this->board->generateMoves(2, 2);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - k - - - - - -
     * - - - - - - r -
     * - - - - - - - -
     * - - - R - - - -
     * - - - - b - - -
     * N - - - - - - -
     * - B - - - - - -
     * R - - - - K - -
     */
    public function test2DiagonalBishopWithCapture()
    {
        $fen = '1k6/6r1/8/3R4/4b3/N7/1B6/R4K2 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(2, 2);

        $target1 = $this->board->getSquareByRankAndFile(3, 3);
        $target2 = $this->board->getSquareByRankAndFile(4, 4);
        $target3 = $this->board->getSquareByRankAndFile(5, 5);
        $target4 = $this->board->getSquareByRankAndFile(6, 6);
        $target5 = $this->board->getSquareByRankAndFile(7, 7);
        $target6 = $this->board->getSquareByRankAndFile(1, 3);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
        ];

        $moveList = $this->board->generateMoves(2, 2);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - - - - - -
     * R - - - P - - -
     * - - P k - - p -
     * - - - - - - - -
     * - - - - B - - -
     * - - - - - - - -
     * - B - r - - r -
     * - - - - - K - -
     * @dataProvider threatMapProvider
     * @param $rank
     * @param $file
     * @param $marked
     */
    public function testThreatGeneration($rank, $file, $marked)
    {
        $fen = '8/R3P3/2Pk2p1/8/4B3/8/1B1r2r1/5K2 w';
        $this->board->initFromFen($fen);

        $originSquare = $this->board->getSquareByRankAndFile(4, 5);
        $threats = $originSquare->getPiece()->getThreats($originSquare);

        $this->assertEquals($marked, $threats->isMarked($rank, $file));
    }

    public function threatMapProvider() {
        return [
            [7, 2, false],
            [6, 3, true],
            [5, 4, true],
            [4, 5, false],
            [5, 6, true],
            [6, 7, true],
            [7, 8, false],
            [3, 6, true],
            [2, 7, true],
            [1, 8, false],
            [3, 4, true],
            [2, 3, true],
            [1, 2, true],
        ];
    }
}