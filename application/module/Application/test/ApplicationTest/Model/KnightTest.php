<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04.09.14
 * Time: 20:38
 */

namespace Application\Model;


use Application\Traits\BoardAware;

class KnightTest extends \PHPUnit_Framework_TestCase {

    use BoardAware;
    public function test8MoveKnight()
    {
        $fen = 'k7/8/8/4n3/8/8/8/K7 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(7, 4);
        $target2 = $this->board->getSquareByRankAndFile(7, 6);
        $target3 = $this->board->getSquareByRankAndFile(6, 7);
        $target4 = $this->board->getSquareByRankAndFile(4, 7);
        $target5 = $this->board->getSquareByRankAndFile(3, 4);
        $target6 = $this->board->getSquareByRankAndFile(3, 6);
        $target7 = $this->board->getSquareByRankAndFile(6, 3);
        $target8 = $this->board->getSquareByRankAndFile(4, 3);

        $moveList = $this->board->generateMoves(5, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8)
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * Here one square out of the 8 is occupied.
     */
    public function test7MoveKnight()
    {
        $fen = '8/3k4/8/4n3/8/8/8/K7 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(7, 6);
        $target2 = $this->board->getSquareByRankAndFile(6, 7);
        $target3 = $this->board->getSquareByRankAndFile(4, 7);
        $target4 = $this->board->getSquareByRankAndFile(3, 4);
        $target5 = $this->board->getSquareByRankAndFile(3, 6);
        $target6 = $this->board->getSquareByRankAndFile(6, 3);
        $target7 = $this->board->getSquareByRankAndFile(4, 3);

        $moveList = $this->board->generateMoves(5, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7)
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test6MoveKnight() {
        $fen = 'k7/4n3/8/8/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(7, 5);

        $target1 = $this->board->getSquareByRankAndFile(8, 7);
        $target2 = $this->board->getSquareByRankAndFile(6, 7);
        $target3 = $this->board->getSquareByRankAndFile(5, 4);
        $target4 = $this->board->getSquareByRankAndFile(5, 6);
        $target5 = $this->board->getSquareByRankAndFile(8, 3);
        $target6 = $this->board->getSquareByRankAndFile(6, 3);

        $moveList = $this->board->generateMoves(7, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test5MoveKnight()
    {
        $fen = '6k1/4n3/8/8/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(7, 5);

        $target1 = $this->board->getSquareByRankAndFile(6, 7);
        $target2 = $this->board->getSquareByRankAndFile(5, 4);
        $target3 = $this->board->getSquareByRankAndFile(5, 6);
        $target4 = $this->board->getSquareByRankAndFile(8, 3);
        $target5 = $this->board->getSquareByRankAndFile(6, 3);

        $moveList = $this->board->generateMoves(7, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test4MoveKnight()
    {
        $fen = 'k7/6n1/8/8/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(7, 7);

        $target1 = $this->board->getSquareByRankAndFile(5, 6);
        $target2 = $this->board->getSquareByRankAndFile(5, 8);
        $target3 = $this->board->getSquareByRankAndFile(8, 5);
        $target4 = $this->board->getSquareByRankAndFile(6, 5);

        $moveList = $this->board->generateMoves(7, 7);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test3MoveKnight()
    {
        $fen = '8/6n1/8/5k2/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(7, 7);

        $target1 = $this->board->getSquareByRankAndFile(5, 8);
        $target2 = $this->board->getSquareByRankAndFile(8, 5);
        $target3 = $this->board->getSquareByRankAndFile(6, 5);

        $moveList = $this->board->generateMoves(7, 7);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test2MoveKnight()
    {
        $fen = '7n/8/8/5k2/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(8, 8);

        $target1 = $this->board->getSquareByRankAndFile(6, 7);
        $target2 = $this->board->getSquareByRankAndFile(7, 6);

        $moveList = $this->board->generateMoves(8, 8);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test1MoveKnight()
    {
        $fen = '7n/5k2/8/8/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(8, 8);

        $target1 = $this->board->getSquareByRankAndFile(6, 7);

        $moveList = $this->board->generateMoves(8, 8);

        $expected = [
            new Move($origin, $target1),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function test0MoveKnight()
    {
        $fen = '7n/5k2/6p1/8/8/8/8/3K4 b';
        $this->board->initFromFen($fen);

        $moveList = $this->board->generateMoves(8, 8);

        $expected = [];

        $this->assertEquals($expected, $moveList);
    }


}