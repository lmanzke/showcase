<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 18.09.14
 * Time: 22:57
 */

namespace Application\Model;


use Application\Model\Piece\Knight;

class MoveTest extends \PHPUnit_Framework_TestCase {

    public function testCapture()
    {
        $piece = $this->getMock('\Application\Model\Piece\Knight', [], [], '', false);
        $origin = new Square(1, 2);
        $origin->setPiece($piece);
        $target = new Square(3, 1);

        $move = new Move($origin, $target);

        $this->assertFalse($move->isCapture());
    }
}