<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 05.09.14
 * Time: 05:09
 */

namespace Application\Model;


use Application\Traits\BoardAware;

class QueenTest extends \PHPUnit_Framework_TestCase {
    use BoardAware;


    public function testFreeQueen()
    {
        $fen = '6k1/8/8/8/4q3/8/8/6K1 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 5);

        $target1 = $this->board->getSquareByRankAndFile(5, 5);
        $target2 = $this->board->getSquareByRankAndFile(6, 5);
        $target3 = $this->board->getSquareByRankAndFile(7, 5);
        $target4 = $this->board->getSquareByRankAndFile(8, 5);

        $target5 = $this->board->getSquareByRankAndFile(5, 6);
        $target6 = $this->board->getSquareByRankAndFile(6, 7);
        $target7 = $this->board->getSquareByRankAndFile(7, 8);

        $target8 = $this->board->getSquareByRankAndFile(4, 6);
        $target9 = $this->board->getSquareByRankAndFile(4, 7);
        $target10 = $this->board->getSquareByRankAndFile(4, 8);

        $target11 = $this->board->getSquareByRankAndFile(3, 6);
        $target12 = $this->board->getSquareByRankAndFile(2, 7);
        $target13 = $this->board->getSquareByRankAndFile(1, 8);

        $target14 = $this->board->getSquareByRankAndFile(3, 5);
        $target15 = $this->board->getSquareByRankAndFile(2, 5);
        $target16 = $this->board->getSquareByRankAndFile(1, 5);

        $target17 = $this->board->getSquareByRankAndFile(3, 4);
        $target18 = $this->board->getSquareByRankAndFile(2, 3);
        $target19 = $this->board->getSquareByRankAndFile(1, 2);

        $target20 = $this->board->getSquareByRankAndFile(4, 4);
        $target21 = $this->board->getSquareByRankAndFile(4, 3);
        $target22 = $this->board->getSquareByRankAndFile(4, 2);
        $target23 = $this->board->getSquareByRankAndFile(4, 1);

        $target24 = $this->board->getSquareByRankAndFile(5, 4);
        $target25 = $this->board->getSquareByRankAndFile(6, 3);
        $target26 = $this->board->getSquareByRankAndFile(7, 2);
        $target27 = $this->board->getSquareByRankAndFile(8, 1);

        $moveList = $this->board->generateMoves(4, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
            new Move($origin, $target11),
            new Move($origin, $target12),
            new Move($origin, $target13),
            new Move($origin, $target14),
            new Move($origin, $target15),
            new Move($origin, $target16),
            new Move($origin, $target17),
            new Move($origin, $target18),
            new Move($origin, $target19),
            new Move($origin, $target20),
            new Move($origin, $target21),
            new Move($origin, $target22),
            new Move($origin, $target23),
            new Move($origin, $target24),
            new Move($origin, $target25),
            new Move($origin, $target26),
            new Move($origin, $target27),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testOneDirectionBlockedQueen()
    {
        $fen = '6k1/8/8/3p4/4q3/8/8/6K1 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 5);

        $target1 = $this->board->getSquareByRankAndFile(5, 5);
        $target2 = $this->board->getSquareByRankAndFile(6, 5);
        $target3 = $this->board->getSquareByRankAndFile(7, 5);
        $target4 = $this->board->getSquareByRankAndFile(8, 5);

        $target5 = $this->board->getSquareByRankAndFile(5, 6);
        $target6 = $this->board->getSquareByRankAndFile(6, 7);
        $target7 = $this->board->getSquareByRankAndFile(7, 8);

        $target8 = $this->board->getSquareByRankAndFile(4, 6);
        $target9 = $this->board->getSquareByRankAndFile(4, 7);
        $target10 = $this->board->getSquareByRankAndFile(4, 8);

        $target11 = $this->board->getSquareByRankAndFile(3, 6);
        $target12 = $this->board->getSquareByRankAndFile(2, 7);
        $target13 = $this->board->getSquareByRankAndFile(1, 8);

        $target14 = $this->board->getSquareByRankAndFile(3, 5);
        $target15 = $this->board->getSquareByRankAndFile(2, 5);
        $target16 = $this->board->getSquareByRankAndFile(1, 5);

        $target17 = $this->board->getSquareByRankAndFile(3, 4);
        $target18 = $this->board->getSquareByRankAndFile(2, 3);
        $target19 = $this->board->getSquareByRankAndFile(1, 2);

        $target20 = $this->board->getSquareByRankAndFile(4, 4);
        $target21 = $this->board->getSquareByRankAndFile(4, 3);
        $target22 = $this->board->getSquareByRankAndFile(4, 2);
        $target23 = $this->board->getSquareByRankAndFile(4, 1);

        $moveList = $this->board->generateMoves(4, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
            new Move($origin, $target11),
            new Move($origin, $target12),
            new Move($origin, $target13),
            new Move($origin, $target14),
            new Move($origin, $target15),
            new Move($origin, $target16),
            new Move($origin, $target17),
            new Move($origin, $target18),
            new Move($origin, $target19),
            new Move($origin, $target20),
            new Move($origin, $target21),
            new Move($origin, $target22),
            new Move($origin, $target23),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function testOneDirectionCaptureQueen()
    {
        $fen = '6k1/8/8/3P4/4q3/8/8/6K1 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 5);

        $target1 = $this->board->getSquareByRankAndFile(5, 5);
        $target2 = $this->board->getSquareByRankAndFile(6, 5);
        $target3 = $this->board->getSquareByRankAndFile(7, 5);
        $target4 = $this->board->getSquareByRankAndFile(8, 5);

        $target5 = $this->board->getSquareByRankAndFile(5, 6);
        $target6 = $this->board->getSquareByRankAndFile(6, 7);
        $target7 = $this->board->getSquareByRankAndFile(7, 8);

        $target8 = $this->board->getSquareByRankAndFile(4, 6);
        $target9 = $this->board->getSquareByRankAndFile(4, 7);
        $target10 = $this->board->getSquareByRankAndFile(4, 8);

        $target11 = $this->board->getSquareByRankAndFile(3, 6);
        $target12 = $this->board->getSquareByRankAndFile(2, 7);
        $target13 = $this->board->getSquareByRankAndFile(1, 8);

        $target14 = $this->board->getSquareByRankAndFile(3, 5);
        $target15 = $this->board->getSquareByRankAndFile(2, 5);
        $target16 = $this->board->getSquareByRankAndFile(1, 5);

        $target17 = $this->board->getSquareByRankAndFile(3, 4);
        $target18 = $this->board->getSquareByRankAndFile(2, 3);
        $target19 = $this->board->getSquareByRankAndFile(1, 2);

        $target20 = $this->board->getSquareByRankAndFile(4, 4);
        $target21 = $this->board->getSquareByRankAndFile(4, 3);
        $target22 = $this->board->getSquareByRankAndFile(4, 2);
        $target23 = $this->board->getSquareByRankAndFile(4, 1);

        $target24 = $this->board->getSquareByRankAndFile(5, 4);

        $moveList = $this->board->generateMoves(4, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
            new Move($origin, $target11),
            new Move($origin, $target12),
            new Move($origin, $target13),
            new Move($origin, $target14),
            new Move($origin, $target15),
            new Move($origin, $target16),
            new Move($origin, $target17),
            new Move($origin, $target18),
            new Move($origin, $target19),
            new Move($origin, $target20),
            new Move($origin, $target21),
            new Move($origin, $target22),
            new Move($origin, $target23),
            new Move($origin, $target24),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * @dataProvider threatMapProvider
     * @param $rank
     * @param $file
     * @param $marked
     */
    public function testThreatGeneration($rank, $file, $marked)
    {
        $fen = '8/R3P3/2Pk2p1/8/1Q2B3/8/1B1r2r1/5K2 w';
        $this->board->initFromFen($fen);

        $originSquare = $this->board->getSquareByRankAndFile(4, 2);
        $threats = $originSquare->getPiece()->getThreats($originSquare);

        $this->assertEquals($marked, $threats->isMarked($rank, $file));
    }

    public function threatMapProvider() {
        return [
            [4, 2, false],
            [5, 2, true],
            [6, 2, true],
            [7, 2, true],
            [8, 2, true],
            [5, 3, true],
            [6, 4, true],
            [4, 3, true],
            [4, 4, true],
            [4, 5, true],
            [4, 6, false],
            [3, 3, true],
            [2, 4, true],
            [1, 5, false],
            [3, 2, true],
            [2, 2, true],
            [1, 2, false],
            [3, 1, true],
        ];
    }
}