<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 05.09.14
 * Time: 05:49
 */

namespace Application\Model;


use Application\Traits\BoardAware;

class KingTest extends \PHPUnit_Framework_TestCase {
    use BoardAware;

    /**
     * - - - - - - - -
     * - - - - - k - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - K - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     */
    public function testFreeKing()
    {
        $fen = '8/5k2/8/8/3K4/8/8/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 4);

        $target1 = $this->board->getSquareByRankAndFile(5, 4);
        $target2 = $this->board->getSquareByRankAndFile(5, 5);
        $target3 = $this->board->getSquareByRankAndFile(4, 5);
        $target4 = $this->board->getSquareByRankAndFile(3, 5);
        $target5 = $this->board->getSquareByRankAndFile(3, 4);
        $target6 = $this->board->getSquareByRankAndFile(3, 3);
        $target7 = $this->board->getSquareByRankAndFile(4, 3);
        $target8 = $this->board->getSquareByRankAndFile(5, 3);

        $moveList = $this->board->generateMoves(4, 4);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * k - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - K - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     */
    public function testCornerKing() {
        $fen = 'k7/8/8/8/3K4/8/8/8 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(8, 1);

        $target1 = $this->board->getSquareByRankAndFile(8, 2);
        $target2 = $this->board->getSquareByRankAndFile(7, 2);
        $target3 = $this->board->getSquareByRankAndFile(7, 1);

        $moveList = $this->board->generateMoves(8, 1);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - - - - - -
     * - - - - - - - -
     * k - - - - - - -
     * - - - - - - - -
     * - - - K - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     */
    public function testRimKing() {
        $fen = '8/8/k7/8/3K4/8/8/8 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(6, 1);

        $target1 = $this->board->getSquareByRankAndFile(7, 1);
        $target2 = $this->board->getSquareByRankAndFile(7, 2);
        $target3 = $this->board->getSquareByRankAndFile(6, 2);
        $target4 = $this->board->getSquareByRankAndFile(5, 2);
        $target5 = $this->board->getSquareByRankAndFile(5, 1);

        $moveList = $this->board->generateMoves(6, 1);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - - - - - -
     * - - r - - - - -
     * k - - - - - - -
     * - - - - - - - -
     * - - - K - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     */

    public function testThreatenedSquares() {
        $fen = '8/2r5/k7/8/3K4/8/8/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(4, 4);

        $target1 = $this->board->getSquareByRankAndFile(5, 4);
        $target2 = $this->board->getSquareByRankAndFile(5, 5);
        $target3 = $this->board->getSquareByRankAndFile(4, 5);
        $target4 = $this->board->getSquareByRankAndFile(3, 5);
        $target5 = $this->board->getSquareByRankAndFile(3, 4);

        $moveList = $this->board->generateMoves(4, 4);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - N - - - -
     * - - - - - - - -
     * - - - - - k - -
     * - - - - P - - -
     * - - - - - - - P
     * - - - - - - - -
     * - B - - - - - -
     * - - - - K - - -
     */
    public function testThreatenedSquaresCombo()
    {
        $fen = '3N4/8/5k2/4P3/7P/8/1B6/4K3 b';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(6, 6);

        $target1 = $this->board->getSquareByRankAndFile(7, 7);
        $target2 = $this->board->getSquareByRankAndFile(6, 7);
        $target3 = $this->board->getSquareByRankAndFile(5, 6);
        $target4 = $this->board->getSquareByRankAndFile(7, 5);

        $moveList = $this->board->generateMoves(6, 6);
        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4)
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - - - - - -
     * - - - k - - - -
     * - - - - - - - -
     * - - - K - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     * - - - - - - - -
     */
    public function testKingClash() {
        $fen = '8/3k4/8/3K4/8/8/8/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 4);

        $target1 = $this->board->getSquareByRankAndFile(5, 5);
        $target2 = $this->board->getSquareByRankAndFile(4, 5);
        $target3 = $this->board->getSquareByRankAndFile(4, 4);
        $target4 = $this->board->getSquareByRankAndFile(4, 3);
        $target5 = $this->board->getSquareByRankAndFile(5, 3);

        $moveList = $this->board->generateMoves(5, 4);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5)
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * r - - -  k -c - r
     * - - - -  - -  - -
     * b - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * R - - -C K -C - R
     */
    public function testCastling1() {
        $fen = 'r3k2r/8/b7/8/8/8/8/R3K2R w KQk';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(1, 5);
        $target1 = $this->board->getSquareByRankAndFile(2, 6);
        $target2 = $this->board->getSquareByRankAndFile(1, 4);
        $target3 = $this->board->getSquareByRankAndFile(2, 4);
        $target4 = $this->board->getSquareByRankAndFile(1, 3);

        $moveList = $this->board->generateMoves(1, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4)
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * r - - -c k bc - r
     * - - - -  - -  - -
     * b - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * R - - -C K -C - R
     */
    public function testCastling2() {
        $fen = 'r3kb1r/8/b7/8/8/8/8/R3K2R b KQkq';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(8, 5);
        $target1 = $this->board->getSquareByRankAndFile(7, 6);
        $target2 = $this->board->getSquareByRankAndFile(7, 5);
        $target3 = $this->board->getSquareByRankAndFile(7, 4);
        $target4 = $this->board->getSquareByRankAndFile(8, 4);
        $target5 = $this->board->getSquareByRankAndFile(8, 3);

        $moveList = $this->board->generateMoves(8, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * r n - -c k bc - r
     * - - - -  - -  - -
     * b - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * R - - -C K -C - R
     */
    public function testCastling3() {
        $fen = 'rn2kb1r/8/b7/8/8/8/8/R3K2R b KQkq';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(8, 5);
        $target1 = $this->board->getSquareByRankAndFile(7, 6);
        $target2 = $this->board->getSquareByRankAndFile(7, 5);
        $target3 = $this->board->getSquareByRankAndFile(7, 4);
        $target4 = $this->board->getSquareByRankAndFile(8, 4);

        $moveList = $this->board->generateMoves(8, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * r - - -c k -c - r
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  Q -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  - -  - -
     * - - - -  K -  - -
     */
    public function testCastling4() {
        $fen = 'r3k2r/8/8/4Q3/8/8/8/4K3 b kq';
        $this->board->initFromFen($fen);
        $origin = $this->board->getSquareByRankAndFile(8, 5);

        $target1 = $this->board->getSquareByRankAndFile(8, 6);
        $target2 = $this->board->getSquareByRankAndFile(7, 6);
        $target3 = $this->board->getSquareByRankAndFile(7, 4);
        $target4 = $this->board->getSquareByRankAndFile(8, 4);

        $moveList = $this->board->generateMoves(8, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
        ];

        $this->assertEquals($expected, $moveList);
    }

    /**
     * - - - - - - - -
     * R - - - P - - -
     * - - P k - - p -
     * - - - - - - - -
     * - - - - B - - -
     * - - - - - - - -
     * - B - r - - r -
     * - - - - - K - -
     * @dataProvider threatMapProvider
     * @param $rank
     * @param $file
     * @param $marked
     */
    public function testThreatGeneration($rank, $file, $marked)
    {
        $fen = '8/R3P3/2Pk2p1/8/4B3/8/1B1r2r1/5K2 w';
        $this->board->initFromFen($fen);

        $originSquare = $this->board->getSquareByRankAndFile(1, 6);
        $threats = $originSquare->getPiece()->getThreats($originSquare);

        $this->assertEquals($marked, $threats->isMarked($rank, $file));
    }

    public function testBrokenPosition() {
        $fen = 'r1k5/6pp/p1pP4/2B1qp2/1K6/P2Q4/1PP3Pb/R7 w';
        $this->board->initFromFen($fen);

        $originSquare = $this->board->getSquareByRankAndFile(4, 2);
        $moveList = $this->board->generateMoves(4, 2);

        $target1 = $this->board->getSquareByRankAndFile(4, 3);
        $target2 = $this->board->getSquareByRankAndFile(3, 2);
        $target3 = $this->board->getSquareByRankAndFile(4, 1);
        $target4 = $this->board->getSquareByRankAndFile(5, 1);

        $expected = [
            new Move($originSquare, $target1),
            new Move($originSquare, $target2),
            new Move($originSquare, $target3),
            new Move($originSquare, $target4),
        ];

        $this->assertEquals($expected, $moveList);
    }

    public function threatMapProvider() {
        return [
            [1, 6, false],
            [2, 7, true],
            [1, 7, true],
            [1, 5, true],
            [2, 5, true],
        ];
    }


}
