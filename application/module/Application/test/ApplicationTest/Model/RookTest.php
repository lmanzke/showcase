<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 05.09.14
 * Time: 03:15
 */

namespace Application\Model;


use Application\Traits\BoardAware;

class RookTest extends \PHPUnit_Framework_TestCase {

    use BoardAware;
    public function testFreeRook() {
        $fen = '1k6/8/8/4R3/8/8/1K6/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(6, 5);
        $target2 = $this->board->getSquareByRankAndFile(7, 5);
        $target3 = $this->board->getSquareByRankAndFile(8, 5);
        $target4 = $this->board->getSquareByRankAndFile(5, 6);
        $target5 = $this->board->getSquareByRankAndFile(5, 7);
        $target6 = $this->board->getSquareByRankAndFile(5, 8);
        $target7 = $this->board->getSquareByRankAndFile(4, 5);
        $target8 = $this->board->getSquareByRankAndFile(3, 5);
        $target9 = $this->board->getSquareByRankAndFile(2, 5);
        $target10 = $this->board->getSquareByRankAndFile(1, 5);
        $target11 = $this->board->getSquareByRankAndFile(5, 4);
        $target12 = $this->board->getSquareByRankAndFile(5, 3);
        $target13 = $this->board->getSquareByRankAndFile(5, 2);
        $target14 = $this->board->getSquareByRankAndFile(5, 1);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
            new Move($origin, $target11),
            new Move($origin, $target12),
            new Move($origin, $target13),
            new Move($origin, $target14),
        ];

        $moveList = $this->board->generateMoves(5, 5);

        $this->assertEquals($expected, $moveList);
    }

    public function testOneDirectionBlockedRook() {
        $fen = '1k6/8/8/3PR3/8/8/1K6/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(6, 5);
        $target2 = $this->board->getSquareByRankAndFile(7, 5);
        $target3 = $this->board->getSquareByRankAndFile(8, 5);
        $target4 = $this->board->getSquareByRankAndFile(5, 6);
        $target5 = $this->board->getSquareByRankAndFile(5, 7);
        $target6 = $this->board->getSquareByRankAndFile(5, 8);
        $target7 = $this->board->getSquareByRankAndFile(4, 5);
        $target8 = $this->board->getSquareByRankAndFile(3, 5);
        $target9 = $this->board->getSquareByRankAndFile(2, 5);
        $target10 = $this->board->getSquareByRankAndFile(1, 5);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
        ];

        $moveList = $this->board->generateMoves(5, 5);

        $this->assertEquals($expected, $moveList);
    }

    public function testOneDirectionCaptureRook() {
        $fen = '1k6/8/8/3pR3/8/8/1K6/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(6, 5);
        $target2 = $this->board->getSquareByRankAndFile(7, 5);
        $target3 = $this->board->getSquareByRankAndFile(8, 5);
        $target4 = $this->board->getSquareByRankAndFile(5, 6);
        $target5 = $this->board->getSquareByRankAndFile(5, 7);
        $target6 = $this->board->getSquareByRankAndFile(5, 8);
        $target7 = $this->board->getSquareByRankAndFile(4, 5);
        $target8 = $this->board->getSquareByRankAndFile(3, 5);
        $target9 = $this->board->getSquareByRankAndFile(2, 5);
        $target10 = $this->board->getSquareByRankAndFile(1, 5);
        $target11 = $this->board->getSquareByRankAndFile(5, 4);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
            new Move($origin, $target9),
            new Move($origin, $target10),
            new Move($origin, $target11),
        ];

        $moveList = $this->board->generateMoves(5, 5);

        $this->assertEquals($expected, $moveList);
    }

    public function testTwoDirectionCaptureRook() {
        $fen = '1k6/8/8/3pR3/4p3/8/1K6/8 w';
        $this->board->initFromFen($fen);

        $origin = $this->board->getSquareByRankAndFile(5, 5);

        $target1 = $this->board->getSquareByRankAndFile(6, 5);
        $target2 = $this->board->getSquareByRankAndFile(7, 5);
        $target3 = $this->board->getSquareByRankAndFile(8, 5);
        $target4 = $this->board->getSquareByRankAndFile(5, 6);
        $target5 = $this->board->getSquareByRankAndFile(5, 7);
        $target6 = $this->board->getSquareByRankAndFile(5, 8);
        $target7 = $this->board->getSquareByRankAndFile(4, 5);
        $target8 = $this->board->getSquareByRankAndFile(5, 4);

        $expected = [
            new Move($origin, $target1),
            new Move($origin, $target2),
            new Move($origin, $target3),
            new Move($origin, $target4),
            new Move($origin, $target5),
            new Move($origin, $target6),
            new Move($origin, $target7),
            new Move($origin, $target8),
        ];

        $moveList = $this->board->generateMoves(5, 5);

        $this->assertEquals($expected, $moveList);
    }

    public function testNoMoveRook() {
        $fen = '1k6/8/4P3/3PRP2/4P3/8/1K6/8 w';
        $this->board->initFromFen($fen);

        $expected = [];

        $moveList = $this->board->generateMoves(5, 5);

        $this->assertEquals($expected, $moveList);
    }

    /**
     * @dataProvider threatMapProvider
     * @param $rank
     * @param $file
     * @param $marked
     */
    public function testThreatGeneration($rank, $file, $marked)
    {
        $fen = '8/R3P3/2Pk2p1/8/4B3/8/1B1r2r1/5K2 w';
        $this->board->initFromFen($fen);

        $originSquare = $this->board->getSquareByRankAndFile(2, 4);
        $threats = $originSquare->getPiece()->getThreats($originSquare);

        $this->assertEquals($marked, $threats->isMarked($rank, $file));
    }

    public function threatMapProvider() {
        return [
            [3, 4, true],
            [4, 4, true],
            [5, 4, true],
            [6, 4, true],
            [7, 4, false],
            [2, 4, false],
            [2, 5, true],
            [2, 6, true],
            [2, 7, true],
            [2, 8, false],
            [1, 4, true],
            [2, 3, true],
            [2, 2, true],
            [2, 1, false],
        ];
    }
}