<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04.09.14
 * Time: 21:12
 */

namespace Application\Enum;


class Direction {
    public static $UP = 2;
    public static $DOWN = 4;

    public static $RIGHT = 3;
    public static $LEFT = 9;

    public static $UPLEFT = 5;
    public static $DOWNRIGHT = 25;

    public static $UPRIGHT = 7;
    public static $DOWNLEFT = 49;

    public static $allDirections = [2, 7, 3, 25, 4, 49, 9, 5]; //clockwise
    public static $horizontals = [9, 3];
    public static $verticals = [2, 4];
    public static $straights = [2, 3, 4, 9];

    public static $topLeftDiagonal = [5, 25];
    public static $topRightDiagonal = [7, 49];

    public static $diagonals = [5, 7, 25, 49];

    public static $castlingFenRepresentation = [3 => 'K', 9 => 'Q', 6 => 'k', 18 => 'q'];

    /**
     * @param int $direction
     * @return bool
     */
    public static function isHorizontal($direction) {
        return ($direction % 3 === 0);
    }

    /**
     * @param int $direction
     * @return bool
     */
    public static function isVertical($direction) {
        return ($direction % 2 === 0);
    }

    /**
     * @param int $direction
     * @return bool
     */
    public static function isTopLeftDiagonal($direction) {
        return ($direction % 5 === 0);
    }

    /**
     * @param int $direction
     * @return bool
     */
    public static function isTopRightDiagonal($direction) {
        return ($direction % 7 === 0);
    }

    /**
     * @param int $direction
     * @return array
     */
    public static function getOrthogonals($direction) {
        if (static::isHorizontal($direction)) {
            return static::$verticals;
        }
        if (static::isVertical($direction)) {
            return static::$horizontals;
        }
        if (static::isTopLeftDiagonal($direction)) {
            return static::$topRightDiagonal;
        }
        if (static::isTopRightDiagonal($direction)) {
            return static::$topLeftDiagonal;
        }
        return [];
    }
}