<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04.09.14
 * Time: 00:44
 */

namespace Application\Enum;


class Color {

    public static $WHITE = 1;
    public static $BLACK = 2;
    public static $BOTH = [1, 2];

    public static function other($color) {
        switch ($color) {
            case static::$WHITE:
                return static::$BLACK;
            default:
                return static::$WHITE;
        }
    }
}