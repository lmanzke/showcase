<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04.09.14
 * Time: 00:45
 */

namespace Application\Enum;


class PieceType {

    public static $PAWN = 1;
    public static $KNIGHT = 2;
    public static $BISHOP = 3;
    public static $ROOK = 4;
    public static $QUEEN = 5;
    public static $KING = 6;
}