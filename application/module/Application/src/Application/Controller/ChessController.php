<?php


/**
 * @author Lucas Manzke <lucas.manzke@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de 
 * @created 30.09.14 16:23
 */

namespace Application\Controller;


use Application\Factory\BoardFactory;
use Application\Model\Board;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;

class ChessController extends AbstractActionController {
    /**
     * @var BoardFactory
     */
    protected $boardFactory;

    /**
     * @var Board
     */

    function __construct($boardFactory)
    {
        $this->boardFactory = $boardFactory;
    }


    public function playAction() {
        $session = new Container('game');
        $session->board = $this->boardFactory->createBoard();
        $session->board->initStandardChess();
    }

    private function validateAndExecute($from, $to, $promotion = null) {
        $result = ['valid' => false];

        $session = new Container('game');
        if (empty($session->board)) {
            return $result;
        }
        /**
         * @var $board Board
         */

        $board = $session->board;
        $this->boardFactory->restoreBoard($board);
        $fromCoordinates = $board->getCoordinates($from);
        $originSquare = $board->getSquareByRankAndFile($fromCoordinates['rank'], $fromCoordinates['file']);

        if (!$originSquare->isOccupied()) {
            return $result;
        }

        if ($originSquare->getPiece()->getColor() !== $board->getSideToMove()) {
            return $result;
        }

        $moveList = $board->generateMovesForSquare($originSquare);

        $toCoordinates = $board->getCoordinates($to);
        foreach ($moveList as $move) {
            if ($move->getTargetSquare()->getRank() !== $toCoordinates['rank']) {
                continue;
            }
            if ($move->getTargetSquare()->getFile() !== $toCoordinates['file']) {
                continue;
            }
            if (!empty($promotion) && $move->getPromotionPieceString() !== $promotion) {
                continue;
            }
            $board->makeMove($move);
            $result['valid'] = true;
            $result['newPos'] = $board->getFen();
            $result['capture'] = $move->isCapture();
            return $result;
        }
        return $result;
    }

    public function moveAction() { //if true, the move will be made
        $from = $this->params('from');
        $to = $this->params('to');
        $promotion = $this->params('promotionPiece');
        $result = $this->validateAndExecute($from, $to, $promotion);

        $this->getResponse()->getHeaders()->addHeaders(array('Content-Type'=>'application/json;charset=UTF-8'));
        return $this->getResponse()->setContent(Json::encode($result));
    }

    public function validmovesAction() {
        $result = [];
        $this->getResponse()->getHeaders()->addHeaders(array('Content-Type'=>'application/json;charset=UTF-8'));

        $session = new Container('game');
        /**
         * @var $board Board
         */

        $board = $session->board;
        $this->boardFactory->restoreBoard($board);

        $square = $this->params('square');

        if (!empty($square)) {
            $coordinates = $board->getCoordinates($square);
            $square = $board->getSquareByRankAndFile($coordinates['rank'], $coordinates['file']);
            if (!$square->isOccupied()) {
                return $this->getResponse()->setContent(Json::encode($result));
            }
            $moveList = $board->getSideToMove() === $square->getPiece()->getColor() ? $board->generateMovesForSquare($square) : [];
        } else {
            $moveList = $board->generateMovesForColor($board->getSideToMove());
        }

        foreach ($moveList as $move) {
            $result[] = ['from' => (string)$move->getOriginSquare(), 'to' => (string)$move->getTargetSquare()];
        }

        return $this->getResponse()->setContent(Json::encode($result));
    }

    public function fenAction() {
        $session = new Container('game');
        /**
         * @var $board Board
         */

        $board = $session->board;
        $this->boardFactory->restoreBoard($board);

        $this->getResponse()->getHeaders()->addHeaders(array('Content-Type'=>'application/json;charset=UTF-8'));
        return $this->getResponse()->setContent(Json::encode(['fen' => $board->getFen()]));
    }

}
