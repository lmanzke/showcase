<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 16.09.14
 * Time: 15:13
 */

namespace Application\Model;

//we do not want this class to be subclassed
final class ThreatGenerator {

    /**
     * @var Board
     */
    protected $board;

    function __construct($board)
    {
        $this->board = $board;
    }

    /**
     * @param int $color
     * @return ThreatMap
     */
    public function generateThreatMap($color) {
        return $this->board->getThreats($color);
    }
} 