<?php


/**
 * @author Lucas Manzke <lucas.manzke@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de 
 * @created 28.08.14 15:18
 */

namespace Application\Model;


use Application\Enum\Color;
use Application\Factory\MoveFactory;
use Application\Factory\PieceFactory;
use Application\Factory\ThreatMapFactory;

abstract class Piece {

    /**
     * @var int
     */
    protected $color;

    /**
     * @var int
     */
    protected $type;

    /**
     * @var MoveFactory
     */
    protected $moveFactory;

    /**
     * @var ThreatMapFactory
     */
    protected $threatMapFactory;

    /**
     * Color and type of the piece will be set at creation time.
     * @param int $color
     * @param MoveFactory $moveFactory
     * @param ThreatMapFactory $threatMapFactory
     */
    function __construct($color, $moveFactory, $threatMapFactory)
    {
        $this->color = $color;
        $this->moveFactory = $moveFactory;
        $this->threatMapFactory = $threatMapFactory;
    }

    /**
     * @return int
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param Move[] $moves
     * @return \Application\Model\ThreatMap
     */
    protected function generateThreatMap($moves) {
        $threatMap = new ThreatMap();
        foreach ($moves as $move) {
            $rank = $move->getTargetSquare()->getRank();
            $file = $move->getTargetSquare()->getFile();
            $threatMap->mark($rank, $file);
        }

        return $threatMap;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    public function getColorString() {
        return $this->color === Color::$WHITE ? 'White' : 'Black';
    }

    public function isWhite() {
        return $this->color === Color::$WHITE;
    }

    /**
     * @param Square $originSquare
     * @param Square|null $enPassantSquare
     * @param array|null $castlingRights
     * @param ThreatGenerator $threatGenerator
     * @return Move[]
     */
    public abstract function generateMoves($originSquare, $enPassantSquare, $castlingRights, $threatGenerator);

    /**
     * @param $originSquare
     * @return ThreatMap
     */
    public abstract function getThreats($originSquare);
    public abstract function getFenRepresentation();
}


