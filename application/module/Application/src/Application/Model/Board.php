<?php


/**
 * @author Lucas Manzke <lucas.manzke@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de 
 * @created 28.08.14 15:31
 */

namespace Application\Model;


use Application\Enum\Color;
use Application\Enum\Direction;
use Application\Enum\PieceType;
use Application\Factory\BoardFactory;
use Application\Factory\MoveFactory;
use Application\Factory\PieceFactory;

class Board {
    /**
     * @var int
     */
    protected $sideToMove;

    /**
     * @var Square[]
     */
    protected $squares;

    /**
     * @var PieceFactory
     */
    protected $pieceFactory;

    /**
     * @var Square
     */
    protected $enPassantSquare;

    /**
     * @var array
     */
    protected $castlingRights;

    /**
     * Number of plies (half moves) since last capture or pawn move
     * @var int
     */
    protected $move50Plies;

    /**
     * number of *full* moves since start (white+black move) + 1
     * @var int
     */
    protected $moveCount;

    /**
     * @param Square[] $squares
     * @param PieceFactory $pieceFactory
     */
    function __construct($squares, $pieceFactory)
    {
        $this->squares = $squares;
        $this->pieceFactory = $pieceFactory;
    }

    /**
     * @param $i
     * @return Square
     */
    public function getSquareByIndex($i) {
        return $this->squares[$i];
    }

    /**
     * @param int $rank
     * @param int $file
     * @return Square
     */
    public function getSquareByRankAndFile($rank, $file) {
        $index = ($rank - 1) * 8 + $file - 1;
        return $this->getSquareByIndex($index);
    }

    public function initStandardChess() {
        $this->squares[0]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$ROOK));
        $this->squares[1]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$KNIGHT));
        $this->squares[2]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$BISHOP));
        $this->squares[3]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$QUEEN));
        $this->squares[4]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$KING));
        $this->squares[5]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$BISHOP));
        $this->squares[6]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$KNIGHT));
        $this->squares[7]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$ROOK));

        for ($i = 16; $i < 48; $i++) {
            $this->squares[$i]->setPiece(null);
        }

        for ($i = 8; $i < 16; $i++) {
            $this->squares[$i]->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$PAWN));
        }

        for ($i = 48; $i < 56; $i++) {
            $this->squares[$i]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$PAWN));
        }

        $this->squares[56]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$ROOK));
        $this->squares[57]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$KNIGHT));
        $this->squares[58]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$BISHOP));
        $this->squares[59]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$QUEEN));
        $this->squares[60]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$KING));
        $this->squares[61]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$BISHOP));
        $this->squares[62]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$KNIGHT));
        $this->squares[63]->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$ROOK));

        $this->sideToMove = Color::$WHITE;

        $this->castlingRights = [
            3 => true,
            6 => true,
            9 => true,
            18 => true,
        ];

        $this->move50Plies = 0;
        $this->moveCount = 1;
    }

    /**
     * @param int $rank
     * @param int $file
     * @return Move[]
     */
    public function generateMoves($rank, $file) {
        $originSquare = $this->getSquareByRankAndFile($rank, $file);
        return $this->generateMovesForSquare($originSquare);
    }

    /**
     * @param Square $originSquare
     * @return Move[]
     */
    public function generateMovesForSquare(Square $originSquare) {
        if (!$originSquare->isOccupied()) {
            return [];
        }

        $result = [];

        $piece = $originSquare->getPiece();
        $threatGenerator = new ThreatGenerator($this);

        $candidateMoves = $piece->generateMoves($originSquare, $this->enPassantSquare, $this->castlingRights, $threatGenerator);

        foreach ($candidateMoves as $candidateMove) {
            if ($this->isLegal($candidateMove)) {
                $result[] = $candidateMove;
            }
        }

        return $result;
    }

    /**
     * This function does not check if the movement of the piece is correct! Only if the arising position is legal.
     * @param Move $move
     * @return bool
     */
    public function isLegal(Move $move) {
        if ($this->sideToMove !== $move->getMovingSide()) {
            return false;
        }

        $move->make();

        $opponentColor = Color::other($move->getMovingSide());
        $threatMap = $this->getThreats($opponentColor);
        $ownKingSquare = $this->findPieceSquare(PieceType::$KING, $move->getMovingSide());

        if (empty($ownKingSquare)) {
            return false;
        }

        $legal = (!$threatMap->isSquareMarked($ownKingSquare));

        $move->undo();

        return $legal;
        //TODO check if pawns are on promotion squares


    }

    /**
     * @param Move $move
     * @return bool
     */
    public function isCheck(Move $move) {
        $move->make();

        $threatMap = $this->getThreats($move->getMovingSide());
        $kingSquare = $this->findPieceSquare(PieceType::$KING, Color::other($move->getMovingSide()));

        $check = $threatMap->isSquareMarked($kingSquare);

        $move->undo();

        return $check;
    }

    /**
     * @param Move $move
     * @return bool
     */
    public function isMate(Move $move) {
        $move->make();
        $nextMoves = $this->generateMovesForColor(Color::other($move->getMovingSide()));

        return empty($nextMoves);
    }



    /**
     * @param int $pieceType
     * @param int $pieceColor
     * @param Square $from
     * @return Square|null
     */
    public function findPieceSquare($pieceType, $pieceColor, $from = null) {
        if (!empty($from)) {
            $rankStart = $from->getRank();
            $fileStart = $from->getFile();
        } else {
            $rankStart = 1;
            $fileStart = 1;
        }

        for ($rank = $rankStart; $rank < 9; $rank++) {
            for ($file = $fileStart; $file < 9; $file++) {
                $square = $this->getSquareByRankAndFile($rank, $file);

                if (!$square->isOccupied()) {
                    continue;
                }

                if ($square->getPiece()->getColor() === $pieceColor && $square->getPiece()->getType() === $pieceType) {
                    return $square;
                }
            }
        }
        return null;
    }

    /**
     * @param int $color
     * @return Move[]
     */
    public function generateMovesForColor($color) {
        $result = [];
        for ($rank = 1; $rank < 9; $rank++) {
            for ($file = 1; $file < 9; $file++) {
                $square = $this->getSquareByRankAndFile($rank, $file);
                if ($square->isOccupied() && $square->getPiece()->getColor() === $color) {
                    foreach($this->generateMovesForSquare($square) as $move) {
                        $result[] = $move;
                    };
                }
            }
        }

        return $result;
    }

    /**
     * @param int $color
     * @return ThreatMap
     */
    public function getThreats($color)
    {
        $result = null;
        foreach ($this->squares as $square) {
            if (!$square->isOccupied()) {
                continue;
            }

            if ($square->getPiece()->getColor() !== $color) {
                continue;
            }

            $result = ($square->getPiece()->getThreats($square)->merge($result));
        }
        return $result;
    }

    /**
     * @param string $fen
     */
    public function initFromFen($fen) {
        $groups = explode(' ', $fen);

        $square = $this->getSquareByRankAndFile(8, 1);
        for ($i = 0; $i < strlen($groups[0]); $i++) {
            if ($groups[0][$i] === '/') {
                $square = $square->moveRight(-7)->moveUp(-1);
                continue;
            }
            if (is_numeric($groups[0][$i])) {
                $square = $square->moveRight($groups[0][$i]);
                continue;
            }
            switch ($groups[0][$i]) {
                case 'R':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$ROOK));
                    break;
                case 'r':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$ROOK));
                    break;
                case 'N':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$KNIGHT));
                    break;
                case 'n':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$KNIGHT));
                    break;
                case 'B':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$BISHOP));
                    break;
                case 'b':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$BISHOP));
                    break;
                case 'K':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$KING));
                    break;
                case 'k':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$KING));
                    break;
                case 'Q':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$QUEEN));
                    break;
                case 'q':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$QUEEN));
                    break;
                case 'P':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$WHITE, PieceType::$PAWN));
                    break;
                case 'p':
                    $square->setPiece($this->pieceFactory->createPiece(Color::$BLACK, PieceType::$PAWN));
                    break;
            }
            $square = $square->moveRight(1);
        }

        switch (lcfirst($groups[1])) {
            case 'w':
                $this->sideToMove = Color::$WHITE;
                break;
            case 'b':
                $this->sideToMove = Color::$BLACK;
                break;
        }

        if (!empty($groups[2])) {
            $this->retrieveCastlingRights($groups[2]);
        }

        if (!empty($groups[3])) {
            $this->retrieveEnPassant($groups[3]);
        }

        if (!empty($groups[4])) {
            $this->move50Plies = (int)$groups[4];
        } else {
            $this->move50Plies = 0;
        }

        if (!empty($groups[5])) {
            $this->moveCount = $groups[5];
        } else {
            $this->moveCount = 1;
        }
    }

    /**
     * @return int
     */
    public function getSideToMove()
    {
        return $this->sideToMove;
    }

    public function retrieveCastlingRights($castlingString) {
        for ($i = 0; $i < strlen($castlingString); $i++) {
            switch ($castlingString[$i]) {
                case 'K':
                    $this->castlingRights[Color::$WHITE * Direction::$RIGHT] = true;
                    break;
                case 'Q':
                    $this->castlingRights[Color::$WHITE * Direction::$LEFT] = true;
                    break;
                case 'k':
                    $this->castlingRights[Color::$BLACK * Direction::$RIGHT] = true;
                    break;
                case 'q':
                    $this->castlingRights[Color::$BLACK * Direction::$LEFT] = true;
                    break;
            }
        }
    }

    public function getCoordinates($squareString) {
        $file = ord($squareString) - 96;
        $rank = (int)$squareString[1];
        return [
            'file' => $file,
            'rank' => $rank
        ];
    }

    /**
     * @param $enPassantString
     */
    private function retrieveEnPassant($enPassantString)
    {
        if (empty($enPassantString) || $enPassantString === '-' || strlen($enPassantString) !== 2) {
            return;
        }

        $file = ord($enPassantString) - 96;
        $rank = $enPassantString[1];
        $this->enPassantSquare = $this->getSquareByRankAndFile($rank, $file);
    }

    /**
     * Generates the Forsyth-Edwards-Notation of the saved position.
     * @return string
     */
    public function getFen() {
        $result = '';
        for ($rank = 8; $rank > 0; $rank--) {
            $number = 0;
            for ($file = 1; $file < 9; $file++) {
                $square = $this->getSquareByRankAndFile($rank, $file);
                if (!$square->isOccupied()) {
                    $number++;
                    continue;
                }
                if ($number === 0) {
                    $result .= $square->getPiece()->getFenRepresentation();
                    continue;
                }
                $result .= $number;
                $result .= $square->getPiece()->getFenRepresentation();
                $number = 0;
            }

            if ($number > 0) {
                $result .= $number;
            }
            if ($rank > 1) {
                $result .= '/';
            }
        }

        $result .= ' ';
        $result .= $this->sideToMove === Color::$WHITE ? 'w' : 'b';

        $result .= ' ';
        $result .= $this->castlingRightsToFen();

        $result .= ' ';
        $result .= empty($this->enPassantSquare) ? '-' : (string)$this->enPassantSquare;

        $result .= ' ';
        $result .= ($this->move50Plies . ' ' . $this->moveCount);

        return $result;
    }

    private function castlingRightsToFen() {
        $result = '';
        foreach (Color::$BOTH as $color) {
            for($i = 1; $i >= 0; $i--) {
                $direction = Direction::$horizontals[$i];
                $index = $color * $direction;
                if (isset($this->castlingRights[$index]) && $this->castlingRights[$index] === true) {
                    $result .= Direction::$castlingFenRepresentation[$index];
                }
            }
        }
        return $result;
    }

    /**
     * @param Move $move
     * @return bool
     */
    public function makeMove(Move $move) {
        if (!$this->isLegal($move)) {
            return false;
        }
        $move->make();
        if ($move->isCapture() || $move->getPiece()->getType() === PieceType::$PAWN) {
            $this->move50Plies = 0;
        } else {
            $this->move50Plies++;
        }
        if (!$move->getPiece()->isWhite()) {
            $this->moveCount++;
        }
        $enPassantSquare = $move->getEnPassantCaptureSquare();
        if (!empty($enPassantSquare)) {
            $this->enPassantSquare = $move->getEnPassantCaptureSquare();
        } else {
            $this->enPassantSquare = null;
        }
        if ($move->getPiece()->getType() === PieceType::$KING) {
            foreach (Direction::$horizontals as $direction) {
                $this->castlingRights[$direction * $this->sideToMove] = false;
            }
        }
        $this->sideToMove = Color::other($this->sideToMove);
        return true;
    }

}
