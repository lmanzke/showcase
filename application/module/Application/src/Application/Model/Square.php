<?php


/**
 * @author Lucas Manzke <lucas.manzke@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de 
 * @created 28.08.14 11:56
 */

namespace Application\Model;




use Application\Enum\Direction;

class Square {
    /**
     * Horizontal lines
     * values: 1 (bottom) - 8 (top)
     * @var int
     */
    protected $rank;

    /**
     * Vertical lines
     * values: 1 (left) - 8 (right)
     * @var int
     */
    //weakrefs to prevent infinite loops due to circular references

    protected $file;

    /**
     * @var \WeakRef
     */
    protected $up;

    /**
     * @var \WeakRef
     */
    protected $down;

    /**
     * @var \WeakRef
     */
    protected $left;

    /**
     * @var \WeakRef
     */
    protected $right;

    /**
     * @var Piece
     */
    protected $piece;

    function __construct($rank, $file)
    {
        $this->rank = $rank;
        $this->file = $file;
    }

    /**
     * @return bool
     */
    public function hasLeft() {
        return !empty($this->left);
    }

    /**
     * @return bool
     */
    public function hasRight() {
        return !empty($this->right);
    }

    /**
     * @return bool
     */
    public function hasUp() {
        return !empty($this->up);
    }

    /**
     * @return bool
     */
    public function hasDown() {
        return !empty($this->down);
    }

    /**
     * @return bool
     */
    public function isOccupied() {
        return !empty($this->piece);
    }

    /**
     * @param \Application\Model\Square $down
     */
    public function setDown($down)
    {
        $this->down = new \WeakRef($down);
    }

    /**
     * @return \Application\Model\Square
     */
    public function getDown()
    {
        return $this->down->get();
    }

    /**
     * @param int $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \Application\Model\Square $left
     */
    public function setLeft($left)
    {
        $this->left = new \WeakRef($left);
    }

    /**
     * @return \Application\Model\Square
     */
    public function getLeft()
    {
        return $this->left->get();
    }

    /**
     * @param \Application\Model\Piece $piece
     */
    public function setPiece($piece)
    {
        $this->piece = $piece;
    }

    /**
     * @return \Application\Model\Piece
     */
    public function getPiece()
    {
        return $this->piece;
    }

    /**
     * @param int $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param \Application\Model\Square $right
     */
    public function setRight($right)
    {
        $this->right = new \WeakRef($right);
    }

    /**
     * @return \Application\Model\Square
     */
    public function getRight()
    {
        return $this->right->get();
    }

    /**
     * @param \Application\Model\Square $up
     */
    public function setUp($up)
    {
        $this->up = new \WeakRef($up);
    }

    /**
     * @return \Application\Model\Square
     */
    public function getUp()
    {
        return $this->up->get();
    }

    /**
     * @return bool
     */
    public function hasUpLeft() {
        return $this->hasLeft() && $this->hasUp();
    }

    /**
     * @return bool
     */
    public function hasUpRight() {
        return $this->hasRight() && $this->hasUp();
    }

    /**
     * @return bool
     */
    public function hasDownLeft() {
        return $this->hasLeft() && $this->hasDown();
    }

    /**
     * @return bool
     */
    public function hasDownRight() {
        return $this->hasRight() && $this->hasDown();
    }

    function __toString()
    {
        return chr($this->file + 96) . $this->rank;
    }

    /**
     * steps: any number (negative or positive)
     * @param int $steps
     * @return Square
     */
    public function moveUp($steps) {
        if ($steps === 0) {
            return $this;
        }

        if ($steps > 0 && $this->hasUp()) {
            return $this->getUp()->moveUp($steps - 1);
        }

        if ($steps < 0 && $this->hasDown()) {
            return $this->getDown()->moveUp($steps + 1);
        }

        return $this; //if code gets here, then we cannot move further
    }

    /**
     * steps: any number (negative or positive)
     * @param int $steps
     * @return Square
     */
    public function moveRight($steps) {
        if ($steps === 0) {
            return $this;
        }

        if ($steps > 0 && $this->hasRight()) {
            return $this->getRight()->moveRight($steps - 1);
        }

        if ($steps < 0 && $this->hasLeft()) {
            return $this->getLeft()->moveRight($steps + 1);
        }

        return $this; //if code gets here, then we cannot move further
    }

    /**
     * A convenience method to handle loops. Goes into a given direction.
     * @param int $direction
     * @param int $steps
     * @return Square
     */
    public function move($direction, $steps) {
        switch ($direction) {
            case Direction::$UP:
                return $this->moveUp($steps);
            case Direction::$RIGHT:
                return $this->moveRight($steps);
            case Direction::$DOWN:
                return $this->moveUp(-$steps);
            case Direction::$LEFT:
                return $this->moveRight(-$steps);
            case Direction::$UPLEFT:
                return $this->moveUp($steps)->moveRight(-$steps);
            case Direction::$UPRIGHT:
                return $this->moveUp($steps)->moveRight($steps);
            case Direction::$DOWNLEFT:
                return $this->moveUp(-$steps)->moveRight(-$steps);
            case Direction::$DOWNRIGHT:
                return $this->moveUp(-$steps)->moveRight($steps);
        }

        //don't know what to do, return myself
        return $this;
    }

    /**
     * @param int $direction
     * @return bool
     */
    public function has($direction) {
        switch ($direction) {
            case Direction::$UP:
                return $this->hasUp();
            case Direction::$RIGHT:
                return $this->hasRight();
            case Direction::$DOWN:
                return $this->hasDown();
            case Direction::$LEFT:
                return $this->hasLeft();
            case Direction::$UPLEFT:
                return $this->hasUpLeft();
            case Direction::$UPRIGHT:
                return $this->hasUpRight();
            case Direction::$DOWNLEFT:
                return $this->hasDownLeft();
            case Direction::$DOWNRIGHT:
                return $this->hasDownRight();
        }

        return false;
    }

    /**
     * Calculates the distance (rank + file difference) between this and another square
     * @param Square $square
     * @return int
     */
    public function getDistance(Square $square) {
        $rankDiff = abs($square->getRank() - $this->rank);
        $fileDiff = abs($square->getFile() - $this->file);
        return $rankDiff + $fileDiff;
    }

    /**
     * Get the direction from this to a target square
     * This only works for horizontal directions at the moment. We do not need more for now.
     * Returns -1 if we are on the same file
     * @param Square $target
     * @return int
     */
    public function getDirection(Square $target) {
        if ($target->getFile() === $this->file) {
            return -1;
        }

        return ($target->getFile() > $this->file) ? Direction::$RIGHT : Direction::$LEFT;
    }
}
