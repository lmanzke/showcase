<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 16:11
 */

namespace Application\Model;


use Application\Enum\Color;
use Application\Enum\PieceType;
use Application\Exception\IllegalArgumentException;

class Move {
    /**
     * @var Square
     */
    protected $originSquare;

    /**
     * @var Square
     */
    protected $targetSquare;

    /**
     * @var Piece
     */
    protected $enPassantClearancePiece;

    /**
     * @var Square
     */
    protected $enPassantClearanceSquare;

    /**
     * @var Square
     */
    protected $enPassantCaptureSquare;

    /**
     * @var Piece
     */
    protected $newPiece;

    /**
     * @var Piece
     */
    protected $piece;

    /**
     * @var Piece
     */
    protected $capturedPiece;

    /**
     * @var int
     */
    protected $movingSide;

    /**
     * @var bool
     */
    protected $isCapture;

    /**
     * @var array
     */
    protected $castlingRights;

    /**
     * @param Square $originSquare
     * @param Square $targetSquare
     * @param Square $enPassantCaptureSquare
     * @param Square $enPassantClearanceSquare
     * @param Piece $newPiece
     * @param array $castlingRights
     * @throws \Application\Exception\IllegalArgumentException
     * @internal param int $newPieceType
     */
    function __construct(Square $originSquare, Square $targetSquare, $enPassantCaptureSquare = null, $enPassantClearanceSquare = null, $newPiece = null, $castlingRights = null)
    {
        if (!$originSquare->isOccupied()) {
            throw new IllegalArgumentException('There is no piece to be moved');
        }

        $this->originSquare = $originSquare;
        $this->targetSquare = $targetSquare;
        $this->enPassantCaptureSquare = $enPassantCaptureSquare;
        $this->enPassantClearanceSquare = $enPassantClearanceSquare;
        if (!empty($enPassantClearanceSquare)) {
            $this->enPassantClearancePiece = $enPassantClearanceSquare->getPiece();
        }

        $this->newPiece = $newPiece;
        $this->piece = $originSquare->getPiece();
        $this->movingSide = $this->piece->getColor();
        $this->capturedPiece = $targetSquare->getPiece();
        $this->isCapture = !empty($this->capturedPiece) || !empty($this->enPassantClearanceSquare);
        $this->castlingRights = $castlingRights;
    }


    /**
     * @return \Application\Model\Square
     */
    public function getOriginSquare()
    {
        return $this->originSquare;
    }

    /**
     * @return string
     */
    public function getPromotionPieceString() {
        if (empty($this->newPiece)) {
            return '';
        }

        switch ($this->newPiece->getType()) {
            case PieceType::$QUEEN:
                return 'q';
            case PieceType::$ROOK:
                return 'r';
            case PieceType::$BISHOP:
                return 'b';
            case PieceType::$KNIGHT:
                return 'n';
        }
    }

    /**
     * @return \Application\Model\Piece
     */
    public function getPiece()
    {
        return $this->piece;
    }

    /**
     * @return \Application\Model\Square
     */
    public function getTargetSquare()
    {
        return $this->targetSquare;
    }

    function __toString()
    {
        return (string)$this->piece . ' ' . (string)$this->originSquare . ' to ' . (string)$this->targetSquare;
    }

    /**
     * @return int
     */
    public function getMovingSide()
    {
        return $this->movingSide;
    }

    /**
     * @return \Application\Model\Piece
     */
    public function getCapturedPiece()
    {
        return $this->capturedPiece;
    }

    /**
     * @return \Application\Model\Square
     */
    public function getEnPassantClearanceSquare()
    {
        return $this->enPassantClearanceSquare;
    }

    public function isCastling() {
        return ($this->piece->getType() === PieceType::$KING)
            && ($this->getOriginSquare()->getDistance($this->getTargetSquare()) === 2)
            && ($this->getOriginSquare()->getRank() === $this->getTargetSquare()->getRank());
    }

    public function make() {
        $this->targetSquare->setPiece($this->piece);
        $this->originSquare->setPiece(null);

        if (!empty($this->enPassantClearanceSquare)) {
            $this->enPassantClearancePiece = $this->enPassantClearanceSquare->getPiece();
            $this->enPassantClearanceSquare->setPiece(null);
        }

        if ($this->isCastling()) {
            $castlingDirection = $this->originSquare->getDirection($this->targetSquare);
            $castlingRookSquare = $this->originSquare->move($castlingDirection, 4); //short castling is less, but we won't fall off
            $newRookSquare = $this->originSquare->move($castlingDirection, 1);
            $newRookSquare->setPiece($castlingRookSquare->getPiece());
            $castlingRookSquare->setPiece(null);
        }

        if ($this->isPromotion()) {
            $this->targetSquare->setPiece($this->newPiece);
        };
    }

    public function isPromotion() {
        if ($this->piece->getType() !== PieceType::$PAWN) {
            return false;
        }
        $whitePromotion = ($this->targetSquare->getRank() === 8) && ($this->getMovingSide() === Color::$WHITE);
        $blackPromotion = ($this->targetSquare->getRank() === 1) && ($this->getMovingSide() === Color::$BLACK);
        return $whitePromotion || $blackPromotion;
    }

    public function undo() {
        $this->originSquare->setPiece($this->piece);
        $this->targetSquare->setPiece($this->capturedPiece);

        if (!empty($this->enPassantClearanceSquare)) {
            $this->enPassantClearanceSquare->setPiece($this->enPassantClearancePiece);
        }

        if ($this->isCastling()) {
            $castlingDirection = $this->originSquare->getDirection($this->targetSquare);
            $castlingRookSquare = $this->originSquare->move($castlingDirection, 4); //short castling is less, but we won't fall off
            $newRookSquare = $this->originSquare->move($castlingDirection, 1);
            $castlingRookSquare->setPiece($newRookSquare->getPiece());
            $newRookSquare->setPiece(null);
        }
    }

    public function isCapture() {
        return $this->isCapture;
    }

    /**
     * @return Square
     */
    public function getEnPassantCaptureSquare()
    {
        return $this->enPassantCaptureSquare;
    }

    /**
     * @return array
     */
    public function getCastlingRights()
    {
        return $this->castlingRights;
    }



}
