<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 08.09.14
 * Time: 19:56
 */

namespace Application\Model;


class ThreatMap {

    /**
     * @var bool[]
     */
    protected $threatened;

    function __construct()
    {
        $this->threatened = [];
        for ($i = 0; $i < 64; $i++) {
            $this->threatened[] = false;
        }
    }

    /**
     * @param int $rank
     * @param int $file
     */
    public function mark($rank, $file) {
        $index = ($rank - 1) * 8 + $file - 1;
        $this->threatened[$index] = true;
    }

    /**
     * @param Square $square
     */
    public function markBySquare($square) {
        $this->mark($square->getRank(), $square->getFile());
    }

    /**
     * @param int $rank
     * @param int $file
     * @param bool $value
     */
    protected function setMarked($rank, $file, $value) {
        $index = ($rank - 1) * 8 + $file - 1;
        $this->threatened[$index] = $value;
    }

    /**
     * @param int $rank
     * @param int $file
     * @return bool
     */
    public function isMarked($rank, $file) {
        $index = ($rank - 1) * 8 + $file - 1;
        return $this->threatened[$index] === true;
    }

    /**
     * @param Square $square
     * @return bool
     */
    public function isSquareMarked(Square $square) {
        return $this->isMarked($square->getRank(), $square->getFile());
    }

    /**
     * @param ThreatMap $otherMap
     * @return ThreatMap
     */
    public function merge($otherMap) {
        if (empty($otherMap)) {
            return $this;
        }

        for ($rank = 1; $rank < 9; $rank++) {
            for ($file = 1; $file < 9; $file++) {
                $isMarked = $this->isMarked($rank, $file) || $otherMap->isMarked($rank, $file);
                $this->setMarked($rank, $file, $isMarked);
            }
        }
        return $this;
    }
} 