<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 17:15
 */

namespace Application\Model\Piece;


use Application\Enum\Direction;
use Application\Enum\PieceType;
use Application\Factory\MoveFactory;
use Application\Model\Move;
use Application\Model\Piece;
use Application\Model\Square;

class Knight extends Piece {
    function __construct($color, $moveFactory, $threatMapFactory)
    {
        parent::__construct($color, $moveFactory, $threatMapFactory);
        $this->type = PieceType::$KNIGHT;
    }

    function __toString()
    {
        return $this->getColorString() . ' Knight';
    }

    /**
     * @param Square $originSquare
     * @param Square $enPassantSquare
     * @param $castlingRights
     * @param ThreatGenerator $threatGenerator
     * @return Move[]
     */
    public function generateMoves($originSquare, $enPassantSquare, $castlingRights, $threatGenerator)
    {
        $result = [];
        foreach (Direction::$straights as $direction) {
            $mainSquare = $originSquare->move($direction, 2);
            foreach (Direction::getOrthogonals($direction) as $orthogonal) {
                $branchSquare = $mainSquare->move($orthogonal, 1);
                if ($originSquare->getDistance($branchSquare) !== 3) {
                    continue;
                }
                if (!$branchSquare->isOccupied() || $branchSquare->getPiece()->getColor() !== $this->color) {
                    $result[] = $this->moveFactory->generateMove($originSquare, $branchSquare);
                }
            }
        }
        return $result;
    }

    public function getThreats($originSquare)
    {
        $result = $this->threatMapFactory->getBlankThreatMap();
        foreach (Direction::$straights as $direction) {
            $mainSquare = $originSquare->move($direction, 2);
            foreach (Direction::getOrthogonals($direction) as $orthogonal) {
                $branchSquare = $mainSquare->move($orthogonal, 1);
                if ($originSquare->getDistance($branchSquare) !== 3) {
                    continue;
                }
                $result->markBySquare($branchSquare);
            }
        }
        return $result;
    }

    public function getFenRepresentation()
    {
        return $this->isWhite() ? 'N' : 'n';
    }
}