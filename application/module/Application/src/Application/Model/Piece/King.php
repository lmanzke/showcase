<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 17:17
 */

namespace Application\Model\Piece;


use Application\Enum\Color;
use Application\Enum\Direction;
use Application\Enum\PieceType;
use Application\Factory\MoveFactory;
use Application\Model\Move;
use Application\Model\Piece;
use Application\Model\Square;
use Application\Model\ThreatGenerator;
use Application\Model\ThreatMap;

class King extends Piece
{

    function __construct($color, $moveFactory, $threatMapFactory)
    {
        parent::__construct($color, $moveFactory, $threatMapFactory);
        $this->type = PieceType::$KING;
    }

    function __toString()
    {
        return $this->getColorString() . ' King';
    }

    /**
     * @param Square $originSquare
     * @param Square $enPassantSquare
     * @param $castlingRights
     * @param ThreatGenerator $threatGenerator
     * @return Move[]
     */
    public function generateMoves($originSquare, $enPassantSquare, $castlingRights, $threatGenerator)
    {
        $threatMap = $threatGenerator->generateThreatMap(Color::other($this->color));

        $result = [];
        foreach (Direction::$allDirections as $direction) {
            if (!$originSquare->has($direction)) {
                continue;
            }
            $currentSquare = $originSquare->move($direction, 1);
            if ($currentSquare->isOccupied() && $currentSquare->getPiece()->getColor() === $this->color) {
                continue;
            }
            if ($threatMap->isMarked($currentSquare->getRank(), $currentSquare->getFile())) {
                continue; //Square is threatened
            }
            if ($currentSquare->isOccupied()) { //capture
                $result[] = $this->moveFactory->generateMove($originSquare, $currentSquare);
                continue; //cannot move through pieces
            }
            $result[] = $this->moveFactory->generateMove($originSquare, $currentSquare);
        }
        foreach ($this->generateCastlingMoves($originSquare, $castlingRights, $threatMap) as $move) {
            $result[] = $move;
        }

        return $result;
    }

    /**
     * @param Square $originSquare
     * @param array $castlingRights
     * @param ThreatMap $threatMap
     * @return Move[]
     */
    private function generateCastlingMoves($originSquare, $castlingRights, $threatMap) {
        $originRank = $this->color === Color::$WHITE ? 1 : 8;
        $originFile = 5;

        if ($threatMap->isMarked($originSquare->getRank(), $originSquare->getFile())) {
            return []; //we are in check, no castling
        }

        if ($originSquare->getRank() !== $originRank) {
            return [];
        }

        if ($originSquare->getFile() !== $originFile) {
            return [];
        }

        $result = [];


        foreach (Direction::$horizontals as $horizontal) {
            $currentSquare = $originSquare;
            $canCastle = true;

            $index = $horizontal * $this->color;
            if (!isset($castlingRights[$index]) || $castlingRights[$index] !== true) {
                continue;
            }
            $distance = 0;
            while ($currentSquare->has($horizontal)) {
                $distance++;
                $currentSquare = $currentSquare->move($horizontal, 1);
                if ($threatMap->isMarked($currentSquare->getRank(), $currentSquare->getFile()) && $distance <= 2) { //king cannot move through check
                    $canCastle = false;
                    break;
                }
                if (!$currentSquare->isOccupied()) {
                    continue;
                }
                if ($currentSquare->getFile() !== 1 && $currentSquare->getFile() !== 8) {
                    $canCastle = false;
                    break;
                }

                if ($currentSquare->getPiece()->getColor() !== $this->color || $currentSquare->getPiece()->getType() !== PieceType::$ROOK) {
                    $canCastle = false;
                    break;
                }
            }

            if ($canCastle) {
                $result[] = $this->moveFactory->generateMove($originSquare, $originSquare->move($horizontal, 2));
            }
        }
        return $result;
    }

    /**
     * @param Square $originSquare
     * @return ThreatMap
     */
    public function getThreats($originSquare)
    {
        $result = $this->threatMapFactory->getBlankThreatMap();
        foreach (Direction::$allDirections as $direction) {
            if (!$originSquare->has($direction)) {
                continue;
            }
            $currentSquare = $originSquare->move($direction, 1);
            $result->markBySquare($currentSquare);
        }
        return $result;
    }

    public function getFenRepresentation()
    {
        return $this->isWhite() ? 'K' : 'k';
    }
}
