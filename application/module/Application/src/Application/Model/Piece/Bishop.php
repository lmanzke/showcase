<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 17:16
 */

namespace Application\Model\Piece;


use Application\Enum\Direction;
use Application\Enum\PieceType;
use Application\Factory\MoveFactory;
use Application\Model\Move;
use Application\Model\Piece;
use Application\Model\Square;
use Application\Model\ThreatGenerator;
use Application\Model\ThreatMap;

class Bishop extends Piece {
    function __construct($color, $moveFactory, $threatMapFactory)
    {
        parent::__construct($color, $moveFactory, $threatMapFactory);
        $this->type = PieceType::$BISHOP;
    }

    function __toString()
    {
        return $this->getColorString() . ' Bishop';
    }

    /**
     * @param Square $originSquare
     * @param Square $enPassantSquare
     * @param array|null $castlingRights
     * @param ThreatGenerator $threatGenerator
     * @return Move[]
     */
    public function generateMoves($originSquare, $enPassantSquare, $castlingRights, $threatGenerator)
    {
        $result = [];
        foreach (Direction::$diagonals as $diagonal) {
            $currentSquare = $originSquare;
            while ($currentSquare->has($diagonal)) {
                $currentSquare = $currentSquare->move($diagonal, 1);
                if ($currentSquare->isOccupied() && $currentSquare->getPiece()->getColor() === $this->color) {
                    break;
                }
                if ($currentSquare->isOccupied()) { //capture
                    $result[] = $this->moveFactory->generateMove($originSquare, $currentSquare);
                    break; //cannot move through pieces
                }
                $result[] = $this->moveFactory->generateMove($originSquare, $currentSquare);
            }
        }
        return $result;
    }

    public function getThreats($originSquare)
    {
        $result = $this->threatMapFactory->getBlankThreatMap();
        foreach (Direction::$diagonals as $diagonal) {
            $currentSquare = $originSquare;
            while ($currentSquare->has($diagonal)) {
                $currentSquare = $currentSquare->move($diagonal, 1);
                $result->markBySquare($currentSquare);

                if ($currentSquare->isOccupied()) {
                    break;
                }
            }
        }
        return $result;
    }

    public function getFenRepresentation()
    {
        return $this->isWhite() ? 'B' : 'b';
    }
}