<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 17:11
 */

namespace Application\Model\Piece;


use Application\Enum\PieceType;
use Application\Factory\MoveFactory;
use Application\Factory\PieceFactory;
use Application\Model\Move;
use Application\Model\Piece;
use Application\Model\Square;
use Application\Model\ThreatMap;

class Pawn extends Piece
{
    /**
     * @var PieceFactory
     */
    protected $pieceFactory;

    function __construct($color, $moveFactory, $pieceFactory, $threatMapFactory)
    {
        parent::__construct($color, $moveFactory, $threatMapFactory);
        $this->type = PieceType::$PAWN;
        $this->pieceFactory = $pieceFactory;
    }


    function __toString()
    {
        return $this->getColorString() . ' ' . 'Pawn'; //TODO replace hard coded strings
    }


    /**
     * @param Square $originSquare
     * @param Square $enPassantSquare
     * @param $castlingRights
     * @param ThreatGenerator $threatGenerator
     * @return Move[]
     */
    public function generateMoves($originSquare, $enPassantSquare, $castlingRights, $threatGenerator)
    {
        return array_merge(
            $this->generateForwardMoves($originSquare),
            $this->generateCaptureMoves($originSquare, $enPassantSquare)
        );

        //TODO add promotion
        //TODO add en passant
    }

    /**
     * @param Square $square
     * @param Square $enPassantSquare
     * @return Move[]
     */
    private function generateCaptureMoves($square, $enPassantSquare) {
        $steps = $this->isWhite() ? 1 : -1;
        $result = [];
        /**
         * @var Square $target
         */
        $targets = [
            $square->moveUp($steps)->moveRight(-1),
            $square->moveUp($steps)->moveRight(1)
        ];
        foreach ($targets as $target) {
            if ($target->getRank() === $square->getRank() || $target->getFile() === $square->getFile()) { //we moved out of the board somewhere
                continue;
            }
            if (!$target->isOccupied() && $target != $enPassantSquare) {
                continue;
            }
            if (!$target->isOccupied()) { // en passant
                $result[] = $this->moveFactory->generateMove($square, $target, null, $enPassantSquare->moveUp(-$steps));
                continue;
            }
            if ($target->getPiece()->getColor() === $this->color) {
                continue;
            }
            if ($target->getRank() === ($this->isWhite() ? 8 : 1)) {
                $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$KNIGHT));
                $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$BISHOP));
                $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$ROOK));
                $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$QUEEN));
            } else {
                $result[] = $this->moveFactory->generateMove($square, $target);
            }
        }
        return $result;
    }

    /**
     * @param Square $square
     * @return array
     */
    private function generateForwardMoves($square)
    {
        $result = [];

        $steps = $this->isWhite() ? 1 : -1; //if white: we move up 1 step, otherwise -1 step up (-> 1 step down)
        $target = $square->moveUp($steps);

        if ($target->isOccupied()) {
            return $result;
        }

        if ($target->getRank() === ($this->isWhite() ? 8 : 1)) {
            $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$KNIGHT));
            $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$BISHOP));
            $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$ROOK));
            $result[] = $this->moveFactory->generateMove($square, $target, null, null, $this->pieceFactory->createPiece($this->color, PieceType::$QUEEN));
        } else {
            $result[] = $this->moveFactory->generateMove($square, $target);
        }

        //TODO replace hard coded values
        if ($square->getRank() !== ($this->isWhite() ? 2 : 7)) {
            return $result;
        }

        $steps *= 2;
        $target = $square->moveUp($steps);

        if (!$target->isOccupied()) {
            $result[] = $this->moveFactory->generateMove($square, $target, $target->moveUp($steps / -2));
        }

        return $result;
    }

    /**
     * @param Square $originSquare
     * @return ThreatMap
     */
    public function getThreats($originSquare)
    {
        $steps = $this->isWhite() ? 1 : -1;

        $result = $this->threatMapFactory->getBlankThreatMap();

        $targets = [
            $originSquare->moveUp($steps)->moveRight(-1),
            $originSquare->moveUp($steps)->moveRight(1)
        ];

        /**
         * @var Square $target
         */

        foreach ($targets as $target) {
            if ($target->getRank() !== $originSquare->getRank() && $target->getFile() !== $originSquare->getFile()) {
                $result->markBySquare($target);
            }
        }

        return $result;
    }


    public function getFenRepresentation()
    {
        return $this->isWhite() ? 'P' : 'p';
    }
}