<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 16:19
 */

namespace Application\Factory;


use Application\Model\Move;
use Application\Model\Piece;
use Application\Model\Square;

class MoveFactory {

    /**
     * @param Square $originSquare
     * @param Square $targetSquare
     * @param Square $enPassantSquare
     * @param Square $enPassantClearanceSquare
     * @param array $castlingRights
     * @param Piece $newPiece
     * @return Move
     */
    public function generateMove($originSquare, $targetSquare, $enPassantSquare = null, $enPassantClearanceSquare = null, $newPiece = null, $castlingRights = null) {
        return new Move($originSquare, $targetSquare, $enPassantSquare, $enPassantClearanceSquare, $newPiece, $castlingRights);
    }
}