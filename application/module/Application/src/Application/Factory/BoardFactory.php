<?php


/**
 * @author Lucas Manzke <lucas.manzke@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de 
 * @created 28.08.14 15:33
 */

namespace Application\Factory;


use Application\Model\Board;
use Application\Model\Square;

class BoardFactory {
    /**
     * @var PieceFactory
     */
    protected $pieceFactory;

    /**
     * @param $pieceFactory
     */
    function __construct($pieceFactory)
    {
        $this->pieceFactory = $pieceFactory;
    }

    /**
     * @return Board
     */
    public function createBoard() {
        /**
         * @var Square[] $squares
         */

        // create the squares
        $squares = [];
        for ($i = 0; $i < 64; $i++) {
            $rank = (int)($i / 8) + 1;
            $file = $i % 8 + 1;

            $squares[$i] = new Square($rank, $file);
        }

        //associate them
        for ($i = 0; $i < 64; $i++) {
            $square = $squares[$i];
            if ($square->getFile() > 1) { // not the left side
                $square->setLeft($squares[$i - 1]);
            }

            if ($square->getFile() < 8) { //not the right side
                $square->setRight($squares[$i + 1]);
            }

            if ($square->getRank() > 1) { //not on bottom
                $square->setDown($squares[$i - 8]);
            }

            if ($square->getRank() < 8) { //not on top
                $square->setUp($squares[$i + 8]);
            }
        }

        return new Board($squares, $this->pieceFactory);
    }

    public function restoreBoard(Board $board) {
        for ($i = 0; $i < 64; $i++) {
            $square = $board->getSquareByIndex($i);
            if ($square->getFile() > 1) { // not the left side
                $square->setLeft($board->getSquareByIndex($i - 1));
            }

            if ($square->getFile() < 8) { //not the right side
                $square->setRight($board->getSquareByIndex($i + 1));
            }

            if ($square->getRank() > 1) { //not on bottom
                $square->setDown($board->getSquareByIndex($i - 8));
            }

            if ($square->getRank() < 8) { //not on top
                $square->setUp($board->getSquareByIndex($i + 8));
            }
        }
    }
} 
