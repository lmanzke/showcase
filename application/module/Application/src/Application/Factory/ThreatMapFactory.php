<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 17.09.14
 * Time: 17:22
 */

namespace Application\Factory;


use Application\Model\ThreatMap;

class ThreatMapFactory {
    public function getBlankThreatMap() {
        return new ThreatMap();
    }
} 