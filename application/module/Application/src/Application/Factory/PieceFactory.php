<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03.09.14
 * Time: 00:31
 */

namespace Application\Factory;


use Application\Enum\PieceType;
use Application\Model\Piece;

class PieceFactory {

    /**
     * @var MoveFactory
     */
    private $moveFactory;

    /**
     * @var ThreatMapFactory
     */
    private $threatMapFactory;

    function __construct(MoveFactory $moveFactory, ThreatMapFactory $threatMapFactory)
    {
        $this->moveFactory = $moveFactory;
        $this->threatMapFactory = $threatMapFactory;
    }

    /**
     * @param int $color
     * @param int $type
     * @return Piece
     */
    public function createPiece($color, $type) {
        switch ($type) {
            case PieceType::$PAWN:
                return new Piece\Pawn($color, $this->moveFactory, $this, $this->threatMapFactory);
            case PieceType::$ROOK:
                return new Piece\Rook($color, $this->moveFactory, $this->threatMapFactory);
            case PieceType::$KNIGHT:
                return new Piece\Knight($color, $this->moveFactory, $this->threatMapFactory);
            case PieceType::$BISHOP:
                return new Piece\Bishop($color, $this->moveFactory, $this->threatMapFactory);
            case PieceType::$QUEEN:
                return new Piece\Queen($color, $this->moveFactory, $this->threatMapFactory);
            case PieceType::$KING:
                return new Piece\King($color, $this->moveFactory, $this->threatMapFactory);
        }
        return null;
    }
} 