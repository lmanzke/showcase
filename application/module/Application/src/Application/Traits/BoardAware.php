<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04.09.14
 * Time: 18:45
 */

namespace Application\Traits;


use Application\Factory\BoardFactory;
use Application\Factory\MoveFactory;
use Application\Factory\PieceFactory;
use Application\Factory\ThreatMapFactory;
use Application\Model\Board;

trait BoardAware {
    /**
     * @var Board
     */
    private $board;

    public function setUp()
    {
        $moveFactory = new MoveFactory();
        $threatMapFactory = new ThreatMapFactory();
        $pieceFactory = new PieceFactory($moveFactory, $threatMapFactory);
        $boardFactory = new BoardFactory($pieceFactory);
        $this->board = $boardFactory->createBoard();
    }
} 